package queue;
/**
 * 
 * @author abdo
 *
 */
public interface IQueue {
    /**
    * Inserts an item at the queue front.
    * @param item object to add to the queue
    */
    public void enqueue(Object item);
    /**
    * Removes the object at the queue rear and returns it.
    * @return first in element
    */
    public Object dequeue();
    /**
    * Tests if this queue is empty.
    * @return true if empty and vice cersa
    */
    public boolean isEmpty();
    /**Returns the number of elements in the queue.
    * @return size of the array
    */
    public int size();
}
