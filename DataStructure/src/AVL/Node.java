package AVL;

public class Node<T> implements INode {

	private Comparable data;
	private Node left;
	private Node right;
	private Node parent;
	private Integer coun = 0, hieght = 1;

	public Node(Comparable data) {
		this.data = data;
		coun++;
	}

	@Override
	public Integer getHieght() {
		return hieght;
	}

	@Override
	public boolean exists(Comparable num) {
		if (data.equals(num))
			return true;
		else if (num.compareTo(data) > 0) {
			if (!hasRight())
				return false;
			else
				return right.exists(num);
		} else {
			if (!hasLeft())
				return false;
			else
				return left.exists(num);
		}
	}

	@Override
	public INode getLeftChild() {
		return left;
	}

	@Override
	public INode getRightChild() {
		return right;
	}

	@Override
	public Comparable<T> getValue() {
		return data;
	}

	@Override
	public void setValue(Comparable value) {
		data = value;
	}

	@Override
	public Comparable getMin() {
		if (!hasLeft())
			return data;
		else
			return left.getMin();
	}

	@Override
	public Comparable getMax() {
		if (!hasRight())
			return data;
		else
			return right.getMax();
	}

	@Override
	public Comparable successor(Comparable num) {
		Node numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.hasRight())
			return numNode.right.getMin();
		else {
			Comparable nodeData = numNode.data;
			numNode = parent(numNode);
			while (!numNode.isRoot() && !parent(numNode).isRoot()
					&& !parent(numNode).equals(parent(parent(numNode)).left)) {
				numNode = parent(numNode);
			}
			if (numNode.isRoot())
				return nodeData;
			return numNode.data;
		}
	}

	@Override
	public Comparable predecessor(Comparable num) {
		Node numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.hasLeft())
			return numNode.left.getMax();
		else {
			Comparable nodeData = numNode.data;
			while (!numNode.isRoot() && !numNode.equals(parent(numNode).right)) {
				numNode = parent(numNode);
			}
			if (numNode.isRoot())
				return nodeData;
			return numNode.data;
		}
	}

	@Override
	public void insert(Comparable data) {
		if (data.compareTo(this.data) > 0) {
			if (!hasRight())
				attachRightChild(data);
			else
				right.insert(data);
		} else if (data.compareTo(this.data) < 0) {
			if (!hasLeft())
				attachLeftChild(data);
			else
				left.insert(data);
		} else
			addVirtualChild();
		updateHieght(this);
		balance(this);
	}

	@Override
	public Boolean delete(Comparable num) {
		Node up = null, toDelete = search(num);
		if (toDelete == null)
			return false;
		else if (toDelete.coun > 1) {
			coun--;
			return true;
		} else {
			if (!toDelete.hasLeft() && !toDelete.hasRight()) {
				up = parent(toDelete);
				toDelete.remove();
				toDelete = null;
			} else if (!toDelete.hasLeft() && toDelete.hasRight()) {
				if (toDelete.isRoot()) {
					toDelete.left = toDelete.right.left;
					toDelete.left.parent = toDelete;
					toDelete.right = toDelete.right.right;
					toDelete.data = toDelete.right.parent.data;
					toDelete.right.parent = null;
					toDelete.right.parent = toDelete;
					up = toDelete;
				} else {
					if (parent(toDelete).right.equals(toDelete)) {
						parent(toDelete).right = toDelete.right;
					} else {
						parent(toDelete).left = toDelete.right;
					}
					toDelete.right.parent = parent(toDelete);
					up = parent(toDelete);
					toDelete = null;
				}
			} else if (toDelete.hasLeft() && !toDelete.hasRight()) {
				if (parent(toDelete) == null) {
					toDelete.right = toDelete.left.right;
					toDelete.right.parent = toDelete;
					toDelete.left = toDelete.left.left;
					toDelete.data = toDelete.left.parent.data;
					toDelete.left.parent = null;
					toDelete.left.parent = toDelete;
					up = toDelete;
				} else {
					if (parent(toDelete).right.equals(toDelete)) {
						parent(toDelete).right = toDelete.left;
					} else {
						parent(toDelete).left = toDelete.left;
					}
					toDelete.left.parent = parent(toDelete);
					up = parent(toDelete);
					toDelete = null;
				}
			} else {
				Node successor = search(successor(num));
				if (successor.hasRight()) {
					if (successor == successor.parent.left) {
						parent(successor).left = successor.right;
					} else {
						parent(successor).right = successor.right;
					}
					successor.right.parent = parent(successor);
					toDelete.data = successor.data;
					up = parent(successor);
					successor = null;
				} else {
					toDelete.data = successor.data;
					up = parent(successor);
					successor.remove();
					successor = null;
				}
			}
			updateDel(up);
			return true;
		}
	}

	private void leftRot(Node node) {
		if (node.isRoot()) {
			Comparable inside = node.data;
			node.data = node.right.data;
			Node ne = new Node(inside);
			if (hasLeft())
				node.left.parent = ne;
			ne.left = node.left;
			if (node.right.hasLeft())
				node.right.left.parent = ne;
			ne.right = node.right.left;
			Node delete = node.right;
			node.right.right.parent = node;
			node.right = node.right.right;
			delete = null;
			node.left = ne;
			ne.parent = node;
			updateHieght(node);
			updateHieght(node.left);
			updateHieght(node.right);
		} else {
			Node temp = node.right;
			temp.parent = node.parent;
			node.parent = temp;
			node.right = temp.left;
			if (temp.hasLeft())
				temp.left.parent = node;
			if (temp.parent.hasRight() && temp.parent.right.equals(node))
				temp.parent.right = temp;
			else
				temp.parent.left = temp;
			temp.left = node;
			updateHieght(node);
			updateHieght(temp);
		}
	}

	private void rightRot(Node node) {
		if (node.isRoot()) {
			Comparable inside = node.data;
			node.data = node.left.data;
			Node ne = new Node(inside);
			if (node.hasRight())
				node.right.parent = ne;
			ne.right = node.right;
			if (node.left.hasRight())
				node.left.right.parent = ne;
			ne.left = node.left.right;
			Node delete = node.left;
			node.left.left.parent = node;
			node.left = node.left.left;
			delete = null;
			node.right = ne;
			ne.parent = node;
			updateHieght(node);
			updateHieght(node.left);
			updateHieght(node.right);
		} else {
			Node temp = node.left;
			temp.parent = node.parent;
			node.parent = temp;
			node.left = temp.right;
			if (temp.hasRight())
				temp.right.parent = node;
			if (temp.parent.hasLeft() && temp.parent.left.equals(node))
				temp.parent.left = temp;
			else
				temp.parent.right = temp;
			temp.right = node;
			updateHieght(node);
			updateHieght(temp);
		}
	}

	@Override
	public void inTraverse() {
		if (hasLeft())
			left.inTraverse();
		System.out.print(data + " ");
		if (hasRight())
			right.inTraverse();
	}

	@Override
	public void preTraverse() {
		System.out.print(data + " ");
		if (hasLeft())
			left.preTraverse();
		if (hasRight())
			right.preTraverse();
	}

	@Override
	public void postTraverse() {
		if (hasLeft())
			left.postTraverse();
		if (hasRight())
			right.postTraverse();
		System.out.print(data + " ");
	}

	private Node parent(Node node) {
		return node.parent;
	}

	private void attachRightChild(Comparable value) {
		right = new Node<>(value);
	}

	private void attachLeftChild(Comparable value) {
		left = new Node<>(value);
	}

	private boolean isRoot() {
		return parent == null;
	}

	private boolean hasLeft() {
		return left != null;
	}

	private boolean hasRight() {
		return right != null;
	}

	private void addVirtualChild() {
		coun++;
	}

	private int blcFactor() {
		return (left == null ? 0 : left.getHieght()) - (right == null ? 0 : right.getHieght());
	}

	private void updateHieght(Node node) {
		node.hieght = 1 + Math.max((!node.hasLeft() ? 0 : node.left.getHieght()),
				(!node.hasRight() ? 0 : node.right.getHieght()));
	}

	private void balance(Node node) {
		int blc = node.blcFactor();
		if (blc == 2) {
			if (node.left.blcFactor() == -1)
				leftRot(node.left);
			rightRot(node);
		} else if (blc == -2) {
			if (node.right.blcFactor() == 1)
				rightRot(node.right);
			leftRot(node);
		}
	}

	// return null if num was not found
	private Node search(Comparable num) {
		if (data.equals(num))
			return this;
		else if (num.compareTo(data) > 0) {
			if (!hasRight())
				return right;
			else
				return right.search(num);
		} else {
			if (!hasLeft())
				return left;
			else
				return left.search(num);
		}
	}

	private void remove() {
		if (this.parent.left != null && this.parent.left.equals(this))
			this.parent.left = null;
		else
			this.parent.right = null;
	}

	private void updateDel(Node toDelete) {
		while (!toDelete.isRoot()) {
			updateHieght(toDelete);
			balance(toDelete);
			toDelete = parent(toDelete);
		}
		updateHieght(toDelete);
		balance(toDelete);
	}

}
