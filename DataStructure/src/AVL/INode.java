package AVL;

public interface INode<T extends Comparable<T>> {
	
	
	
	
	/**
	 * Returns the left child of the current element/node in the heap tree
	 * 
	 * @return INode wrapper to the left child of the current element/node
	 */
	INode<T> getLeftChild();

	/**
	 * Returns the right child of the current element/node in the heap tree
	 * 
	 * @return INode wrapper to the right child of the current element/node
	 */
	INode<T> getRightChild();

	/**
	 * Set/Get the value of the current node
	 * 
	 * @return Value of the current node
	 */
	T getValue();
	/**
	 * set value of this node
	 * @param value
	 */
	void setValue(T value);

	Boolean delete(Comparable num);

	void insert(Comparable data);

	boolean exists(Comparable num);

	Integer getHieght();

	Comparable getMin();

	Comparable getMax();

	Comparable successor(Comparable num);

	Comparable predecessor(Comparable num);

	void preTraverse();

	void postTraverse();

	void inTraverse();
}