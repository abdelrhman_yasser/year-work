package eg.edu.alexu.csd.assignments.iceHockey.cs42;

import java.awt.Point;
import java.util.Comparator;

public class cmp implements Comparator<Point> {
    
    @Override
    public final int compare(final Point p1, final Point p2) {
        if (p1.x != p2.x) {
            return p1.x - p2.x;
        } else {
            return p1.y - p1.y;
        }
    }
}
