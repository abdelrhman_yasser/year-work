import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class secondaryLevel {

	private ArrayList<Integer> data;
	private Integer[] hashMap;
	private Random r = new Random();

	private int max,k, p, collisions, rebuilds;

	public secondaryLevel() {
		data = new ArrayList<>();
		collisions = 0;
		rebuilds = 0;
	}

	public void add(int d) {
		if(!data.contains(d)){
			data.add(d);
			collisions++;
		}
		max = Math.max(max, d);
	}
	

	public boolean tri() throws Exception {

		hashMap = new Integer[(int) Math.pow(collisions, 2)];

		boolean[] token = new boolean[(int) Math.pow(collisions, 2)];
		Arrays.fill(token, false);

		k = -1;
		while (k <= 0)
			k = r.nextInt(10000);
		
		p = PrimeGenerator.getInstance().getPrimeGreaterN(max);
		
		
		
		
		for (int i : data) {
			int key = (int)(((long)k%p * (long)i%p) % p) % hashMap.length;
			if (token[key]){
				rebuilds++;
				return false;
			}
			token[key] = true;
			hashMap[key] = i;
		}
		return true;

	}
	
	public void printLevel(FileOutputStream f) throws IOException{
		String s = "K = " + k + " P = "+ p + " rebuilds = "+ rebuilds +" Elements >>";
		f.write(s.getBytes());
		for(int i=0 ; i<hashMap.length ; i++){
			if(hashMap[i]!=null)s = " "+ hashMap[i] +" ";
			else s = " - ";
			f.write(s.getBytes());
		}
		
	}

}
