package linkedList.cs42;

import linkedList.ILinkedList;

public class Slinked implements ILinkedList {

    private slistnode head = new slistnode();
    private slistnode tail = new slistnode();
    private int size;

    public Slinked() {
        this.size = 0;
        this.tail.setNext(null);
        this.head.setNext(tail);
    }

    /**.
     * (non-Javadoc)
     * 
     * @see eg.edu.alexu.csd.datastructure.linkedList.ILinkedList#add(int,
     * java.lang.Object)
     */
    @Override
    public final void add(final int index, final Object element) {
        if (index < 0 || index > this.size ) {
            throw new RuntimeException(String.valueOf(index)+" "+String.valueOf(this.size));
        } else {
            slistnode iterator = this.head;
            for (int i = 0; i < index; i++) {
                iterator = iterator.getNext();
            }
            slistnode newnode = new slistnode();
            newnode.setNext(iterator.getNext());
            // newnode.setIndex(index);
            newnode.setItem(element);
            iterator.setNext(newnode);
            // if (index != size) {
            // slistnode iterator2 = newnode;
            // for (int i = index; i < size; i++) {
            // iterator2 = iterator2.getNext();
            // iterator2.setIndex(iterator2.getIndex() + 1);
            // }
            // }
            this.size++;
        }
    }
    @Override
    public final void add(final Object element) {
        slistnode newnode = new slistnode();
        newnode.setItem(element);
        if (this.size == 0) {
            this.head.setNext(newnode);
            newnode.setNext(this.tail);
            // newnode.setIndex(this.size);
            this.size++;
        } else {
            slistnode iterator = this.head;
            for (int i = 0; i < this.size; i++) {
                iterator = iterator.getNext();
            }
            iterator.setNext(newnode);
            newnode.setNext(this.tail);
            // newnode.setIndex(this.size);
            this.size++;
        }
    }

    @Override
    public final Object get(final int index) {
        slistnode iterator = this.head;
        if (index > this.size - 1 || index < 0) {
            throw new RuntimeException("");
        }
        if (index == 0) {
            return this.head.getNext().getItem();
        } else {
            for (int i = 0; i < index + 1; i++) {
                iterator = iterator.getNext();
            }
            return iterator.getItem();
        }
    }

    @Override
    public final void set(final int index, final Object element) {
        slistnode iterator = this.head;
        if (index > this.size - 1 || index < 0) {
            throw new RuntimeException("");
        }
        if (index == 0) {
            this.head.getNext().setItem(element);
        } else {
            for (int i = 0; i < index + 1; i++) {
                iterator = iterator.getNext();
            }
            iterator.setItem(element);
        }
    }

    @Override
    public final void clear() {
        slistnode iterator = this.head;
        iterator.getNext();
        for (int i = 0; i <= this.size; i++) {
            slistnode remove = iterator;
            iterator = iterator.getNext();
            remove.setNext(null);
        }
        this.size = 0;
    }
    /**.
     * print all list
     */
    public final void printall() {
        slistnode iterator = this.head;
        iterator.getNext();
        for (int i = 0; i < this.size; i++) {
            iterator = iterator.getNext();
            System.out.println("element--->" + iterator.getItem());
        }
        System.out.print(this.size);
    }

    @Override
    public final boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public final void remove(final int index) {
        if (index > this.size - 1 || index < 0 || this.size == 0) {
            throw new RuntimeException("");
        }
        slistnode iterator = this.head;
        for (int i = 0; i < index; i++) {
            iterator = iterator.getNext();
        }
        if (index == this.size - 1) {
            slistnode remove = iterator.getNext();
            iterator.setNext(null);
            remove = null;
        } else if (index == 0) {
            slistnode remove = this.head.getNext();
            this.head.setNext(this.head.getNext().getNext());
            remove = null;
            // for (int i = 0; i < size - 1; i++) {
            // iterator = iterator.getNext();
            // // iterator.setIndex(iterator.getIndex() - 1);
            // }
        } else {
            slistnode remove = iterator.getNext();
            slistnode iterator2 = remove;
            for (int i = index; i < this.size - 1; i++) {
                iterator2 = iterator2.getNext();
                // iterator2.setIndex(iterator2.getIndex() - 1);
            }
            iterator.setNext(iterator.getNext().getNext());
            remove = null;
        }
        this.size--;

    }

    @Override
    public final int size() {
        return this.size;
    }

    @Override
    public final ILinkedList sublist(final int fromIndex, final int toIndex) {
        Slinked newlist = new Slinked();
        int i=0;
        slistnode iterator = this.head;
        iterator = iterator.getNext();
        if (fromIndex > this.size - 1 || fromIndex < 0 
                || toIndex > this.size - 1 || toIndex < 0) {
            throw new RuntimeException("");
        }
        for( i=0 ; i<fromIndex ; i++){
            iterator = iterator.getNext();            
        }
        newlist.add(iterator.getItem());
        for(int j=i ; i<toIndex ; i++){
            iterator = iterator.getNext();            
            newlist.add(iterator.getItem());
        }
        return newlist;
    }

    @Override
    public final boolean contains(final Object o) {
        boolean found = false;
        slistnode iterator = this.head;
        for (int i = 0; i < this.size; i++) {
            iterator = iterator.getNext();
            if (iterator.getItem().equals(o)) {
                found = true;
                i = this.size + 1;
            }
        }
        return found;
    }
}