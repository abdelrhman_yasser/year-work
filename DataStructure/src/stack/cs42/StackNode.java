package stack.cs42;
/**
 * 
 * @author abdo
 *
 */
public class StackNode {
	private Object item;
	private int index; 
	private StackNode next;
	/**
	 * 
	 * @param nindex index of position to be modified
	 */
	public final void setIndex(final int nindex) {
		this.index = nindex;
	}
	/**
	 * 
	 * @return index of node
	 */
	public final int getIndex() {
		return this.index;
	}
	/**
	 * 
	 * @return object stored in node
	 */
	public final Object getItem() {
		return this.item;
	}
	/**
	 * 
	 * @param nitem item to be stored in node
	 */
	public final void setItem(final Object nitem) {
		this.item = nitem;
	}
	/**
	 * 
	 * @return node of next 
	 */
	public final StackNode getNext() {
		return this.next;
	}
	/**
	 * 
	 * @param nnext node to be stored in next node
	 */
    
	public final void setNext(final StackNode nnext) {
		this.next = nnext;
	}
}
