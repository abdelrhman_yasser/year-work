import java.util.ArrayList;
import java.util.Random;

public class PrimeGenerator {

	private static PrimeGenerator instance;
	int[] nums = new int[1000007];
	boolean[] hasFactor = new boolean[1000007];
	ArrayList<Integer> primes = new ArrayList<>(); 
	Random r = new Random();
	
	
	private PrimeGenerator() {

		for (int i = 2; i < 1000007; i++)
			nums[i] = i;
		for (int i = 2; i * i < 1000007; i++) {
			if (hasFactor[i])
				continue;
			else
				for (int j = i * i; j < 1000007; j += i)
					hasFactor[j] = true;
		}
		
		for (int i = 2; i < 1000007; i++)
			if(!hasFactor[i])
				primes.add(i);
	}

	public static PrimeGenerator getInstance() {
		if (instance == null)
			instance = new PrimeGenerator();
		return instance;
	}

	public int getPrimeGreaterN(int n) throws Exception {
		
		if(n<2)return 1;
		
		n++;
		while(hasFactor[n])n++;
		int beg = primes.indexOf(n);
		
		
		return primes.get(r.nextInt(primes.size()-beg)+beg);
		
	}

}
