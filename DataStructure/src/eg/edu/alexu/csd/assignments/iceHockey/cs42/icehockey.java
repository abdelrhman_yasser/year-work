package eg.edu.alexu.csd.assignments.iceHockey.cs42;

import java.awt.Point;
import java.util.ArrayList;
/**
 * 
 * @author abdo
 *
 */
public class icehockey implements eg.edu.alexu.csd.assignments.iceHockey.IPlayersFinder {

    ArrayList<Point> pos;
    int x = 0, y = 0, coun = 0, minr = 100000, minc = 100000, maxr = -1, maxc = -1;

    @Override
    public final Point[] findPlayers(final String[] photo, final int team, final int threshold) {
        x = 0;
        y = 0;
        coun = 0;
        minr = 100000;
        minc = 100000;
        maxr = -1;
        maxc = -1;
        // handling null array
        pos = new ArrayList<>();
        if (photo == null || photo.length == 0) {
            return new Point[0];
        }
        // null visited array
        boolean[][] vis = new boolean[photo.length][photo[0].length()];
        for (int i = 0; i < (int) photo.length; i++) {
            for (int j = 0; j < photo[0].length(); j++) {
                vis[0][0] = false;
            }
        }
        // recursively found locations of teams and store them in array list of
        // points
        for (int i = 0; i < (int) photo.length; i++) {
            for (int j = 0; j < (int) photo[0].length(); j++) {
                if (photo[i].charAt(j) == Integer.toString(team).charAt(0) 
                        && !vis[i][j]) {
                    rotate(i, j, photo, vis, team);
                    if (coun * 4 >= threshold) {
                        Point po = new Point(x, y);
                        pos.add(po);
                    }
                    coun = 0;
                    minr = 100000;
                    minc = 100000;
                    maxr = -1;
                    maxc = -1;
                }
            }
        }
        // sort
        cmp sort = new cmp();
        pos.sort(sort);
        // make needed point array by coping its element from array list
        Point[] need = new Point[(int) pos.size()];
        for (int i = 0; i < (int) pos.size(); i++) {
            need[i] = new Point(pos.get(i).x, pos.get(i).y);
        }
        pos.clear();
        return need;
    }

    public final void rotate(final int r, final int c, final String[] photo, final boolean[][] vis, final int team) {
        if (r < 0 || c < 0 || r >= photo.length || c >= photo[0].length()
                || photo[r].charAt(c) != Integer.toString(team).charAt(0) || vis[r][c]) {
            return;
        } else {
            vis[r][c] = true;
            minc = Math.min(minc, c);
            maxc = Math.max(maxc, c);
            minr = Math.min(minr, r);
            maxr = Math.max(maxr, r);
            x = (maxc + minc + 1);
            y = (maxr + minr + 1);
            coun++;
            rotate(r + 1, c, photo, vis, team);
            rotate(r, c + 1, photo, vis, team);
            rotate(r - 1, c, photo, vis, team);
            rotate(r, c - 1, photo, vis, team);
        }
    }
}
