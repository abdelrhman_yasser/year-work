package linkedList.cs42;
/**
 * 
 * @author abdo
 *
 */
public class PolynomialNode {
    private int exp;
    private int coff;
    /**
     * 
     * @param nelement set element coff
     */
    public final void set_coff(final int nelement) {
        this.coff = nelement;
    }
    /**
     * 
     * @param element set exp of coff
     */
    public final void set_exp(final int element) {
        this.exp = element;
    }
    /**
     * 
     * @return cofficient
     */
    public final int get_coff() {
        return this.coff;
    }
    /**
     * 
     * @return power
     */
    public final int get_exp() {
        return this.exp;
    }
    /**
     * 
     * @param num value of variable
     * @return value computed
     */
    public final float get_value(float num) {
        return (float) (Math.pow(num, this.exp) * this.coff);
    }
}
