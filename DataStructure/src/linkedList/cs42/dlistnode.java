package linkedList.cs42;
/**
 * 
 * @author abdo
 *
 */
public class dlistnode {
    private Object item;
    //private int index;
    private dlistnode next;
    private dlistnode prev;

    /**
     * 
     * @return previous node
     */
    public final dlistnode getPrev() {
        return prev;
    }

    /**
     * 
     * @param nprev
     *            set previous node
     */
    public final void setPrev(final dlistnode nprev) {
        this.prev = nprev;
    }
    /**
     * 
     * @param nindex set index
     *//*
    public final void setIndex(final int nindex) {
        this.index = nindex;
    }
    *//**
     * 
     * @return index of node
     *//*
    public final int getIndex() {
        return this.index;
    }
    *//**
     * 
     * @return item needed
     */
    public final Object getItem() {
        return this.item;
    }
    /**
     * 
     * @param nitem change item choosen
     */
    public final void setItem(final Object nitem) {
        this.item = nitem;
    }
    /**
     * 
     * @return next node
     */
    public final dlistnode getNext() {
        return this.next;
    }
    /**
     * 
     * @param nnext set next node
     */
    public final void setNext(final dlistnode nnext) {
        this.next = nnext;
    }
}
