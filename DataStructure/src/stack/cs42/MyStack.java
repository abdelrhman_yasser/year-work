package stack.cs42;

import linkedList.cs42.Slinked;
import stack.IStack;
/**
 * 
 * @author abdo
 *
 */
public class MyStack implements IStack {
    Slinked list = new Slinked();
    int top = 0;
    /**.
     * @param index postion to add in
     * @param element object to add
     * add an element in specified index
     */
    public final void add(final int index, final Object element) {
        if (index > top) {
            throw new RuntimeException();
        }
        list.add(top - index, element);
    }
    /**
     * @return object in this node
     */
    public final Object pop() {
        Object temp = list.get(0);
        list.remove(0);
        top--;
        return temp;
    }
    /**
     * @return peek of the list
     */
    public final Object peek() {
        return list.get(0);
    }
    /**
     * @param element the object to be pushed in list 
     */
    public final void push(final Object element) {
        list.add(0, element);
        top++;
    }
    /**
     * @return true if empty and vice versa
     */
    public final boolean isEmpty() {
        if (list.size() == 0) {
            return true;
        }
        return false;
    }
    /**
     * @return size of list
     */
    public final int size() {
        return list.size();
    }
}
