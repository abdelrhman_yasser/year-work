import java.math.BigInteger;

public class math {

    public math() {
        // TODO Auto-generated constructor stub
    }
    
    
    private String aPowbModn1(String a , String b , String n ){
        BigInteger ba = new BigInteger(a);
        BigInteger bb = new BigInteger(b);
        BigInteger bn = new BigInteger(n);
        if(a.toString().equals("0"))
            return "1";
        BigInteger i;
        for(i=new BigInteger("1") ; i.compareTo(bb)==-1 ; i=i.add(BigInteger.ONE) ){
            ba=ba.multiply(new BigInteger(a));
        }
        return ba.mod(bn).toString();
    }
    
    private String aPowbModn2(String a , String b , String n ){
        BigInteger ba = new BigInteger(a);
        BigInteger bb = new BigInteger(b);
        BigInteger bn = new BigInteger(n);
        if(a.toString().equals("0"))
            return "1";
        for(BigInteger i=new BigInteger("1") ; i.compareTo(bb)==-1 ; i=i.add(BigInteger.ONE) ){
            ba.mod(bn);
            ba=ba.multiply(new BigInteger(a));
            ba.mod(bn);
        }
        return ba.mod(bn).toString();
    }
    
    private String aPowbModn3(String a , String b , String n ){
        BigInteger ba = new BigInteger(a);
        BigInteger bb = new BigInteger(b);
        BigInteger bn = new BigInteger(n);
        return expopower(ba,bb,bn).toString();
    }
    
    private BigInteger expopower(BigInteger ba,BigInteger bb,BigInteger bn){
        if(bb.compareTo(BigInteger.ZERO)==0)
            return BigInteger.ONE;
        BigInteger ans = expopower(ba, bb.shiftRight(1), bn);
        if(bb.mod(new BigInteger("2")).compareTo(BigInteger.ZERO)==0)
            return (ans.mod(bn).multiply(ans.mod(bn))).mod(bn);
        else
            return (ba.mod(bn).multiply((ans.mod(bn).multiply(ans.mod(bn))).mod(bn))).mod(bn);
    }
    
    private String modInverse (String a , String n){
        return pow(new BigInteger(a) , new BigInteger(n).subtract(BigInteger.ONE).subtract(BigInteger.ONE)).toString();
    }
    
    private BigInteger pow(BigInteger ba,BigInteger bb){
        if(bb.compareTo(BigInteger.ZERO)==0)
            return BigInteger.ONE;
        BigInteger ans = pow(ba, bb.shiftRight(1));
        if(bb.mod(new BigInteger("2")).compareTo(BigInteger.ZERO)==0)
            return ans.multiply(ans);
        else
            return ba.multiply(ans.multiply(ans));
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
//        math M = new math();
//        Scanner S = new Scanner(System.in);
//        chineseRemindar C = new chineseRemindar(S.nextInt());
//        System.out.println(C.multiMOdM(S.nextInt(), S.nextInt()));
//        System.out.println(M.aPowbModn1(S.next(), S.next(), S.next()));
//        System.out.println(M.aPowbModn2(S.next(), S.next(), S.next()));
//        System.out.println(M.aPowbModn3(S.next(), S.next(), S.next()));
//        System.out.println(M.modInverse(S.next(), S.next()));
    } 

}
