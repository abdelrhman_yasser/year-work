package AVL;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Dictionary implements IDictionary{

	IAVLTree tree = new avlTree<>();
	Integer size = 0 ;
	
	@Override
	public void load(File file) {
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(in.hasNext()){
			size++;
			tree.insert(in.next());
		}
	}

	@Override
	public boolean insert(String word) {
		if(exists(word))
			return false;
		tree.insert(word);
		size++;
		return true;	
	}

	@Override
	public boolean exists(String word) {
		return tree.search(word);
	}

	@Override
	public boolean delete(String word) {
		boolean deleted = tree.delete(word);
		if(deleted)size--;
		return deleted;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int height() {
		return tree.getTree().getHieght();
	}

}
