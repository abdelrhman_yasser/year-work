package binarySearchTree;

import AVL.AVLTree1;

public class BST {

	private Integer data;
	private int coun = 0;
	private BST left;
	private BST right;
	private BST parent;

	private BST(BST parent) {
		this.parent = parent;
	}

	public BST() {
	}

	public void insert(int num) {
		if (data == null) {
			data = num;
			coun++;
		} else if (num > data) {
			if (right == null)
				right = new BST(this);
			right.insert(num);
		} else if (num < data) {
			if (left == null)
				left = new BST(this);
			left.insert(num);
		} else
			coun++;
		
	}

	public void inTraverse() {
		if (left != null)
			left.inTraverse();
		System.out.print(data + " ");
		if (right != null)
			right.inTraverse();
	}

	public void preTraverse() {
		System.out.print(data + " ");
		if (left != null)
			left.preTraverse();
		if (right != null)
			right.preTraverse();
	}

	public void postTraverse() {
		if (left != null)
			left.postTraverse();
		if (right != null)
			right.postTraverse();
		System.out.print(data + " ");
	}

	public int getMin() {
		if (this.left == null)
			return data;
		else
			return left.getMin();
	}

	public int getMax() {
		if (this.right == null)
			return data;
		else
			return right.getMax();
	}

	// return null if num was not found
	private BST search(int num) {
		if (data.equals(num))
			return this;
		else if (num > data) {
			if (right == null)
				return right;
			else
				return right.search(num);
		} else {
			if (left == null)
				return left;
			else
				return left.search(num);
		}
	}

	// key->num of successor of node null if there's no num->key for node
	public Integer successor(int num) {
		BST numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.right != null)
			return numNode.right.getMin();
		else {
			int nodeData = numNode.data;
			numNode = parent(numNode);
			while (parent(numNode) != null && parent(parent(numNode)) != null && !parent(numNode).equals(parent(parent(numNode)).left)) {
				numNode = parent(numNode);
			}
			if (parent(numNode) == null)
				return nodeData;
			return numNode.data;
		}
	}

	// key->num of successor of node null if there's no num->key for node
	public Integer predecessor(int num) {
		BST numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.left != null)
			return numNode.left.getMax();
		else {
			int nodeData = numNode.data;
			while (parent(numNode) != null && !numNode.equals(parent(numNode).right)) {
				numNode = parent(numNode);
			}
			if (parent(numNode) == null)
				return nodeData;
			return numNode.data;
		}
	}

	private BST parent(BST node) {
		return node.parent;
	}
	
	private void remove(){
		if(this.parent.left.equals(this))this.parent.left=null;
		else this.parent.right=null;
	}

	public Boolean delete(int num) {
		BST toDelete = search(num);
		if (toDelete == null)
			return false;
		if (toDelete.left == null && toDelete.right == null) {
			toDelete.remove();
			toDelete = null;
		} else if (toDelete.left == null && toDelete.right != null) {
			if (parent(toDelete) == null) {
				toDelete.left = toDelete.right.left;
				toDelete.left.parent = toDelete;
				toDelete.right = toDelete.right.right;
				toDelete.data = toDelete.right.parent.data;
				toDelete.right.parent = null;
				toDelete.right.parent = toDelete;
			} else {
				if (parent(toDelete).right.equals(toDelete)) {
					parent(toDelete).right = toDelete.right;
				} else {
					parent(toDelete).left = toDelete.right;
				}
				toDelete.right.parent = parent(toDelete);
				toDelete =null;
			}
		} else if (toDelete.left != null && toDelete.right == null) {
			if (parent(toDelete) == null) {
				toDelete.right = toDelete.left.right;
				toDelete.right.parent = toDelete;
				toDelete.left = toDelete.left.left;
				toDelete.data = toDelete.left.parent.data;
				toDelete.left.parent = null;
				toDelete.left.parent = toDelete;
			} else {
				if (parent(toDelete).right.equals(toDelete)) {
					parent(toDelete).right = toDelete.left;
				} else {
					parent(toDelete).left = toDelete.left;
				}
				toDelete.left.parent = parent(toDelete);
				toDelete = null;
			}
		} else {
			BST successor = search(successor(num));
			if (successor.right != null) {
				if (successor == successor.parent.left) {
					parent(successor).left = successor.right;
				} else {
					parent(successor).right = successor.right;
				}
				successor.right.parent = parent(successor);
				toDelete.data = successor.data;
				successor = null;
			}else{
				toDelete.data = successor.data ;
				successor = null;
			}
		}
		return true;
	}

	public static void main(String args[]) {
		BST tree = new BST();
		tree.insert(15);
		tree.insert(5);
		tree.insert(16);
		tree.insert(3);
		tree.insert(12);
		tree.insert(20);
		tree.insert(10);
		tree.insert(13);
		tree.insert(18);
		tree.insert(23);
		tree.insert(6);
		tree.insert(7);
		tree.inTraverse();
		System.out.println();
		System.out.println(tree.getMin());
		System.out.println(tree.getMax());
		System.out.println(tree.successor(3));
		tree.delete(5);
		System.out.println(tree.successor(10));
		tree.inTraverse();
	}

}
