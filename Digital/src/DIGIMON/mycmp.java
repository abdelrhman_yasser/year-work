package DIGIMON;

import java.util.Comparator;
/**
 * 
 * @author DIGIMON
 *
 */
public class mycmp implements Comparator<String>{
    /**
     * sorter for input according number of ones
     */
    @Override
    public int compare(String o1, String o2) {
        int coun1=0 , coun2=0 ;
        for(int i=0 ; i<o1.length() ; i++){
            if(o1.charAt(i)=='1'){
                coun1++;
            }
            if(o2.charAt(i)=='1'){
                coun2++;
            }
        }
        return coun1-coun2;
    }
}
