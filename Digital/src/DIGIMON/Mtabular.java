package DIGIMON;

import java.util.ArrayList;
import java.util.Arrays;
/**
 * 
 * @author DIGIMON
 *
 */
public class Mtabular implements Itabular {
    ArrayList<Integer> result = new ArrayList();
    ArrayList<String> seq = new ArrayList<>();
    ArrayList<ArrayList<Integer>> indeces = new ArrayList<>();
    ArrayList<String> tempseq = new ArrayList<>();
    ArrayList<ArrayList<Integer>> tempindeces = new ArrayList<>();
    ArrayList<String> left = new ArrayList<>();
    ArrayList<ArrayList<Integer>> leftindeces = new ArrayList<>();
    boolean flag = true;

    /**
     * it turn binary number to its decimal form.
     * @param bin the binary number to be converted.
     * @return decimal form
     */
    @Override
    public int turn_decimal(String bin) {
        // System.out.println(bin);
        int pow = 1, sum = 0;
        for (int i = bin.length() - 1; i >= 0; i--) {
            if (bin.charAt(i) == '1') {
                sum += pow;
            }
            pow *= 2;
        }
        return sum;
    }
    /**
     * it takes decimal number and turn it to its binary form
     * @param num the decimal number to be converted
     * @param maxdiv maximum number of divisions
     * @return binary form  
     */
    @Override
    public String binary(int num, int maxdiv) {
        StringBuilder build = new StringBuilder();
        int coun = 0;
        for (int i = 0; i < maxdiv; i++) {
            String re = String.valueOf(num % 2);
            if (re == "1") {
                coun++;
            }
            build.append(re);
            num /= 2;
        }
        return build.reverse().toString();
    }
    /**
     * get max number of division by two repeats.
     * @param largenum max number in the sequence
     * @return counter of number of repeats 
     */
    @Override
    public int get_max_repeat(int largenum) {
        int coun = 0;
        while (largenum > 0) {
            largenum /= 2;
            coun++;
        }
        return coun;
    }
    /**
     * it generates the prime imolicants
     * @param input sequence of implicants 
     * @param maxdiv
     * @see get_max_repeat
     * @return prime implicants
     */
    @Override
    public ArrayList<String> prime_implicants(ArrayList<Integer> input, int maxdiv) {

        /**
         * <a> intializing first tabular
         */
        int coun = 0;
        for (int i = 0; i < input.size(); i++) {
            seq.add(binary(input.get(i), maxdiv));
        }
        mycmp sort = new mycmp();
        seq.sort(sort);
        for (int i = 0; i < seq.size(); i++) {
            ArrayList<Integer> temp = new ArrayList<>();
            temp.add(turn_decimal(seq.get(i)));
            indeces.add(temp);
        }

        /**
         * <a>getting all prime implicants
         */
        boolean flag = true;
        while (!tempseq.isEmpty() || flag) {
            flag = false;
            tempseq.clear();
            tempindeces.clear();
            Boolean[] token = new Boolean[seq.size()];
            Arrays.fill(token, Boolean.FALSE);
            /**
             * <a> iterate over all current implicant to minimize them
             */
            for (int i = 0; i < seq.size(); i++) {
                for (int j = i + 1; j < seq.size(); j++) {
                    int onechange = 0, indexchange = 0;
                    for (int k = 0; k < seq.get(i).length(); k++) {
                        if (seq.get(i).charAt(k) != seq.get(j).charAt(k)) {
                            onechange++;
                            indexchange = k;
                        }
                    }
                    if (onechange == 1) {
                        StringBuilder str = new StringBuilder();
                        str.append(seq.get(i));
                        str.setCharAt(indexchange, '-');
                        tempseq.add(str.toString());
                        ArrayList<Integer> temp = new ArrayList<>();
                        for (int k = 0; k < indeces.get(i).size(); k++) {
                            temp.add(indeces.get(i).get(k));
                        }
                        for (int k = 0; k < indeces.get(j).size(); k++) {
                            temp.add(indeces.get(j).get(k));
                        }
                        tempindeces.add(temp);
                        token[i] = true;
                        token[j] = true;
                    }
                }
            }
            /**
             * <a> it trues the token implicants
             */
            for (int i = 0; i < indeces.size(); i++) {
                for (int k = 0; k < tempindeces.size(); k++) {
                    int count = 0;
                    for (int j = 0; j < indeces.get(0).size(); j++) {
                        for (int l = 0; l < tempindeces.get(0).size(); l++) {
                            if (indeces.get(i).get(j) == tempindeces.get(k).get(l)) {
                                count++;
                                l = tempindeces.get(0).size();
                            }
                        }
                    }
                    if (count == indeces.get(0).size()) {
                        token[i] = true;
                    }
                }
            }
            /**
             * remove similar implicants
             */
            ArrayList<Integer> removelist = new ArrayList<>();
            for (int i = 0; i < tempseq.size(); i++) {
                for (int j = i + 1; j < tempseq.size(); j++) {
                    if (tempseq.get(i).equals(tempseq.get(j))) {
                        removelist.add(i);
                    }
                }
            }
            int counup = 0;
            for (int i = 0; i < removelist.size(); i++) {
                int ri = removelist.get(i);
                tempseq.remove(ri - counup);
                tempindeces.remove(ri - counup);
                counup++;
            }
            /**
             * append untoken to left array
             */
            for (int i = 0; i < seq.size(); i++) {
                if (!token[i]) {
                    left.add(0, seq.get(i));
                    leftindeces.add(0, indeces.get(i));
                }
            }
            /**
             * copy temp indeces and seq to origin lists
             */
            if (!tempseq.isEmpty()) {
                seq.clear();
                for (int i = 0; i < tempseq.size(); i++) {
                    seq.add(tempseq.get(i));
                }
                indeces.clear();
                for (int i = 0; i < tempindeces.size(); i++) {
                    ArrayList<Integer> temp = tempindeces.get(i);
                    indeces.add(temp);
                }
            }
        }
        return left;
    }
    /**
     * it generate most simplified implicant by using complete search
     * @param copy which is prime implicants 
     * @see prime_implicants
     * @param a visited array for recursive complete search
     * @return most simplified implicant 
     */
    @Override
    public void MSPE(int most, int begin, int index, ArrayList<ArrayList<Integer>> copy, boolean[] visited) {
        if (!flag || index > copy.size() || index == copy.size() || begin == most) {//
            return;
        }
        {
            visited[index] = true;
            MSPE(most, begin + 1, index + 1, copy, visited);
            if (complete(visited, copy) && flag) {
                for (int j = 0; j < visited.length; j++) {
                    if (visited[j]) {
                        result.add(j);
                    }
                }
                flag = false;
            }
            visited[index] = false;
            MSPE(most, begin, index + 1, copy, visited);
        }
    }
    /**
     * its used in hashing the table for finding most simplified
     * @param visited a boolean array used in MSPE
     * @see MSPE 
     * @return true if it has found MSPE false otherwise
     */
    @Override
    public boolean complete(boolean[] visited, ArrayList<ArrayList<Integer>> copy) {
        boolean end = true;
        ArrayList<ArrayList<Integer>> temp = new ArrayList<>();
        ArrayList<Integer> tempi = new ArrayList<>();

        for (int i = 0; i < copy.size(); i++) {
            ArrayList<Integer> cat = (ArrayList<Integer>) copy.get(i).clone();
            temp.add(cat);
        }

        for (int i = 0; i < visited.length; i++) {
            if (visited[i]) {
                tempi.add(i);
            }
        }

        for (int large = 0; large < tempi.size(); large++) {
            for (int i = 0; i < temp.get(tempi.get(large)).size(); i++) {
                int dash = temp.get(tempi.get(large)).get(i);
                for (int j = 0; j < temp.size(); j++) {
                    for (int k = 0; k < temp.get(j).size(); k++) {
                        if (temp.get(j).get(k) == dash) {
                            temp.get(j).set(k, -1);
                        }
                    }
                }
            }
        }

        for (int j = 0; j < temp.size(); j++) {
            for (int k = 0; k < temp.get(j).size(); k++) {
                if (temp.get(j).get(k) != -1) {
                    end = false;
                }
            }
        }
        return end;
    }
}
