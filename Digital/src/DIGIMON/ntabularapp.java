package DIGIMON;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.sun.rowset.FilteredRowSetImpl;
import java.awt.Scrollbar;
import java.awt.ScrollPane;
import java.awt.Point;
/**
 * 
 * @author DIGIMON
 *
 */
public class ntabularapp {

    private JFrame frame;
    private JTextField txtInputImplicants;
    private JTextField txtPrimeImplicants;
    private JTextField txtSimplifiedImplicants;
    private JTextField textField;
    private JTextField txtDontCares;
    private JTextField textField_2;
    private Mtabular evaluate = new Mtabular();

    /**
     * <a>Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ntabularapp window = new ntabularapp();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the tabular app.
     */
    public ntabularapp() {
        initialize();
    }

    /**
     * Initialize the contents of the frame of GUI.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
        frame.setResizable(false);
        /*try {
            frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new URL(
                    "http://www.forge-clan.de/files/images/bg4.jpg")))));
        } catch (IOException e3) {
            JOptionPane.showMessageDialog(null, "no network");
        }*/
        frame.getContentPane().setBackground(Color.GRAY);
        frame.getContentPane().setLayout(null);
        frame.setResizable(false);
        
        txtInputImplicants = new JTextField();
        txtInputImplicants.setBounds(32, 88, 282, 63);
        txtInputImplicants.setHorizontalAlignment(SwingConstants.LEFT);
        txtInputImplicants.setEditable(false);
        txtInputImplicants.setFont(new Font("Palatino Linotype", Font.ITALIC, 28));
        txtInputImplicants.setText("  INPUT IMPLICANTS ");
        txtInputImplicants.setForeground(new Color(0, 0, 255));
        txtInputImplicants.setBackground(UIManager.getColor("TextField.darkShadow"));
        frame.getContentPane().add(txtInputImplicants);
        txtInputImplicants.setColumns(10);

        txtDontCares = new JTextField();
        txtDontCares.setBounds(489, 88, 177, 63);
        txtDontCares.setText("  Don't Cares");
        txtDontCares.setHorizontalAlignment(SwingConstants.LEFT);
        txtDontCares.setForeground(Color.BLUE);
        txtDontCares.setFont(new Font("Palatino Linotype", Font.ITALIC, 28));
        txtDontCares.setEditable(false);
        txtDontCares.setColumns(10);
        txtDontCares.setBackground(UIManager.getColor("TextField.darkShadow"));
        frame.getContentPane().add(txtDontCares);

        txtPrimeImplicants = new JTextField();
        txtPrimeImplicants.setBounds(32, 311, 282, 63);
        txtPrimeImplicants.setEditable(false);
        txtPrimeImplicants.setText(" PRIME IMPLICANTS ");
        txtPrimeImplicants.setForeground(Color.BLUE);
        txtPrimeImplicants.setFont(new Font("Palatino Linotype", Font.ITALIC, 28));
        txtPrimeImplicants.setColumns(10);
        txtPrimeImplicants.setBackground(UIManager.getColor("TextField.darkShadow"));
        frame.getContentPane().add(txtPrimeImplicants);

        txtSimplifiedImplicants = new JTextField();
        txtSimplifiedImplicants.setBounds(32, 750, 357, 63);
        txtSimplifiedImplicants.setEditable(false);
        txtSimplifiedImplicants.setText(" SIMPLIFIED IMPLICANT ");
        txtSimplifiedImplicants.setForeground(Color.BLUE);
        txtSimplifiedImplicants.setFont(new Font("Palatino Linotype", Font.ITALIC, 28));
        txtSimplifiedImplicants.setColumns(10);
        txtSimplifiedImplicants.setBackground(UIManager.getColor("TextField.darkShadow"));
        frame.getContentPane().add(txtSimplifiedImplicants);

        textField = new JTextField();
        textField.setBounds(32, 185, 384, 42);
        textField.setToolTipText("input ");
        textField.setFont(new Font("Lucida Fax", Font.BOLD, 20));
        frame.getContentPane().add(textField);
        textField.setColumns(10);
        frame.setBounds(100, 100, 1092, 983);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        textField_2 = new JTextField();
        textField_2.setBounds(489, 185, 384, 42);
        textField_2.setToolTipText("input ");
        textField_2.setFont(new Font("Lucida Fax", Font.BOLD, 20));
        textField_2.setColumns(10);
        frame.getContentPane().add(textField_2);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(32, 403, 1001, 334);
        textArea.setFont(new Font("Courier New", Font.BOLD, 21));
        frame.getContentPane().add(textArea);

        JTextArea textArea_1 = new JTextArea();
        textArea_1.setBounds(32, 836, 1001, 53);
        textArea_1.setWrapStyleWord(true);
        textArea_1.setFont(new Font("Courier New", Font.BOLD, 20));
        frame.getContentPane().add(textArea_1);
        
        JLabel lblNewLabel = new JLabel("Wrong ");
        lblNewLabel.setForeground(new Color(255, 51, 0));
        lblNewLabel.setVisible(false);
        lblNewLabel.setBackground(UIManager.getColor("CheckBoxMenuItem.selectionBackground"));
        lblNewLabel.setBounds(206, 261, 667, 37);
        lblNewLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 18));
        frame.getContentPane().add(lblNewLabel);

        JButton btnNewButton = new JButton(" SIMULATE");
        btnNewButton.setBounds(32, 261, 141, 37);
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                String input1 = textField.getText();
                String input2 = textField_2.getText();
                
                boolean flagcheck = true;
                int maxm = 0;
                boolean wronginput = false;
                StringBuilder build = new StringBuilder();
                ArrayList<Integer> inputdec = new ArrayList<>();
                ArrayList<Integer> dont = new ArrayList<>();
                ArrayList<Integer> orig = new ArrayList<>();

                try {
                    /**
                     * <a> take input from text box 
                     */
                    for (int i = 0; i < input1.length(); i++) {
                        if (Character.isDigit(input1.charAt(i))) {
                            build.append(input1.charAt(i));
                        } else {
                            maxm = Math.max(maxm, Integer.valueOf(build.toString()));
                            // System.out.println(Integer.valueOf(build.toString()));
                            inputdec.add(Integer.valueOf(build.toString()));
                            orig.add(Integer.valueOf(build.toString()));
                            build.delete(0, build.length());
                        }
                    }
                    if (!input1.isEmpty()) {
                        maxm = Math.max(maxm, Integer.valueOf(build.toString()));
                        orig.add(Integer.valueOf(build.toString()));
                        inputdec.add(Integer.valueOf(build.toString()));
                    }
                    build.delete(0, build.length());

                    /**
                     * <a> take input from text box don't care
                     */
                    for (int i = 0; i < input2.length(); i++) {
                        if (Character.isDigit(input2.charAt(i))) {
                            build.append(input2.charAt(i));
                        } else {
                            maxm = Math.max(maxm, Integer.valueOf(build.toString()));
                            // System.out.println(Integer.valueOf(build.toString()));
                            dont.add(Integer.valueOf(build.toString()));
                            inputdec.add(Integer.valueOf(build.toString()));
                            build.delete(0, build.length());
                        }
                    }
                    if (!input2.isEmpty()) {
                        dont.add(Integer.valueOf(build.toString()));
                        maxm = Math.max(maxm, Integer.valueOf(build.toString()));
                        inputdec.add(Integer.valueOf(build.toString()));
                    }
                    build.delete(0, build.length());
                } catch (NumberFormatException e) {
                    wronginput = true;
                }

                /**
                 * <a> check if input is ok
                 */
                int repeat = 0;boolean flag =true;
                boolean flagrepeat = false;
                for (int i = 0; i < orig.size() && flag ; i++) {
                    for (int j = 0; j < dont.size() && flag ; j++) {
                        if (orig.get(i) == dont.get(j)) {
                            repeat = orig.get(i);
                            flagrepeat = true;
                            flag=false;
                        }
                    }
                }
                flag=true;
                for (int i = 0; i < orig.size()&& flag ; i++) {
                    for (int j = i+1; j < orig.size()&& flag ; j++) {
                        if (orig.get(i) == orig.get(j)) {
                            repeat = orig.get(i);
                            flagrepeat = true;
                            i = orig.size();
                            j = orig.size();
                            flag=false;
                        }

                    }
                }
                /**
                 * <a> checking.
                 */
                if (flagrepeat) {
                    textArea.setText("");
                    textArea_1.setText("");
                    lblNewLabel.setText("wrong input form no "+repeat+" was repeated");
                    lblNewLabel.setVisible(true);
                    flagrepeat=false;
                }
                else if(orig.isEmpty()){
                    textArea.setText("0");
                    textArea_1.setText("0");
                }
                else if(wronginput){
                    textArea.setText("");
                    textArea_1.setText("");
                    lblNewLabel.setText("WRONG INPUT FORM !");
                    lblNewLabel.setVisible(true);
                    wronginput=false;
                }
                /**
                 *  <a> get prime implicants
                 */
                else if (orig.size() == 1) {
                    lblNewLabel.setVisible(false);
                    if (Integer.valueOf(inputdec.get(0)) == 1) {
                        textArea.setText("A");
                        textArea_1.setText("A");
                    }
                    else if(Integer.valueOf(inputdec.get(0)) == 0){
                        textArea.setText("A'");
                        textArea_1.setText("A'");
                    }
                    else {
                        String ne = new String();
                        String implicant = evaluate.binary(Integer.valueOf(inputdec.get(0)),
                                evaluate.get_max_repeat(Integer.valueOf(inputdec.get(0))));
                        char first = 'A';
                        for(int j=0 ;j<implicant.length() ; j++)
                        {
                            if(implicant.charAt(j)=='1')
                            {
                                build.append(first);
                            }
                            else if (implicant.charAt(j)=='0'){
                                build.append(first);
                                build.append('\'');
                            }
                           first++;
                        }
                        textArea.setText(build.toString());
                        textArea_1.setText(build.toString());
                    }
                } else {
                    lblNewLabel.setVisible(false);
                    evaluate.prime_implicants(inputdec, evaluate.get_max_repeat(maxm));
                    ArrayList<String> outputprime = evaluate.left;
                    ArrayList<ArrayList<Integer>> copy = evaluate.leftindeces;
                    build.delete(0, build.length());
                    for (int i = 0; i < outputprime.size(); i++) {
                        build.append("implicant" + (i + 1) + ": " + outputprime.get(i) + " & indeces: " + copy.get(i)
                                + '\n');
                    }
                    textArea.setText(build.toString());
                    build.delete(0, build.length());

                    /**
                     * <a> most simplified is made her
                     */
                    boolean[] visited = new boolean[copy.size()];
                    Arrays.fill(visited, false);
                    for (int i = 0; i < dont.size(); i++) {
                        for (int j = 0; j < copy.size(); j++) {
                            for (int k = 0; k < copy.get(j).size(); k++) {
                                if (copy.get(j).get(k) == dont.get(i)) {
                                    copy.get(j).set(k, -1);
                                }
                            }
                        }
                    }
                    for (int i = 0; i < outputprime.size(); i++) {
                        evaluate.MSPE(i + 1, 0, 0, copy, visited);
                        if (!evaluate.result.isEmpty()) {
                            i = outputprime.size();
                        }
                    }
                    for (int i = 0; i < evaluate.result.size(); i++) {
                        String implicant = evaluate.left.get(evaluate.result.get(i));
                        char first = 'A';
                        for(int j=0 ;j<implicant.length() ; j++)
                        {
                            if(implicant.charAt(j)=='1')
                            {
                                build.append(first);
                            }
                            else if (implicant.charAt(j)=='0'){
                                build.append(first);
                                build.append('\'');
                            }
                           first++;
                        }
                        if (i != evaluate.result.size() - 1) {
                            build.append(" + ");
                        }
                        first++;
                    }
                    textArea_1.setText(build.toString());

                    /**
                     * <a> rebuild all arrays
                     */
                    evaluate.left.clear();
                    evaluate.leftindeces.clear();
                    evaluate.result.clear();
                    evaluate.seq.clear();
                    evaluate.tempseq.clear();
                    evaluate.indeces.clear();
                    evaluate.tempindeces.clear();
                    evaluate.result.clear();
                    evaluate.flag = true;
                }
            }
        });
        btnNewButton.setBackground(UIManager.getColor("TextArea.selectionBackground"));
        btnNewButton.setFont(new Font("Impact", Font.BOLD, 18));
        frame.getContentPane().add(btnNewButton);
        
        
        JLabel lblNewLabel_1 = new JLabel("Insert Input as : 1,2,3,4,5,6,7");
        lblNewLabel_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
        lblNewLabel_1.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
        lblNewLabel_1.setForeground(new Color(51, 204, 51));
        lblNewLabel_1.setBounds(32, 33, 282, 42);
        frame.getContentPane().add(lblNewLabel_1);
        
        
    }
}
