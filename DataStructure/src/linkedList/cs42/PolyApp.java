package linkedList.cs42;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
/**
 * 
 * @author abdo
 *
 */
public class PolyApp {

    private JFrame frame;
    private MyPolynomial p1 = new MyPolynomial();

    /**
     * @param args it takes arguments of invoke 
     * Launch the application.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    PolyApp window = new PolyApp();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public PolyApp() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */

    private void initialize() {

        frame = new JFrame();
        frame.setResizable(false);
        frame.getContentPane().setBackground(new Color(51, 204, 153));
        frame.setBounds(100, 100, 1082, 431);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("POLY APP");

        JLabel lblNewLabel = new JLabel("choose an option");
        lblNewLabel.setBackground(new Color(153, 204, 204));
        lblNewLabel.setFont(new Font("Ravie", Font.PLAIN, 18));
        lblNewLabel.setForeground(Color.RED);
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setBounds(352, 13, 214, 43);
        frame.getContentPane().add(lblNewLabel);
        try {
            frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new URL(
                    "http://www.gettyimages.pt/gi-resources/images/Homepage" 
                            + "/Hero/PT/PT_hero_42_153645159.jpg")))));
        } catch (IOException e3) {
            JOptionPane.showMessageDialog(null, "no network");
        }

        JList list = new JList();
        list.setForeground(new Color(70, 130, 180));
        list.setBackground(SystemColor.activeCaption);
        list.setFont(new Font("Ravie", Font.BOLD | Font.ITALIC, 27));
        list.setModel(new AbstractListModel() {
            String[] values = new String[] { " 1 - Set a polynomial variable",
                    " 2 - Print the value of a polynomial variable",
                    " 3 - Add two polynomials",
                    " 4 - Subtract two polynomials",
                    " 5 - Multiply two polynomials",
                    " 6 - Evaluate a polynomial at some p" };

            public int getSize() {
                return values.length;
            }

            public Object getElementAt(final int index) {
                return values[index];
            }
        });
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setBorder(new BevelBorder(BevelBorder.RAISED,
                Color.BLUE, null, null, null));
        list.setBounds(106, 48, 862, 240);
        frame.getContentPane().add(list);
        ///////////////////////////////////////////////
        ///////////////////////////////////////////////
        JButton btnNewButton = new JButton("1");
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent arg0) {
                Object[] possibleValues = { 'A', 'B', 'C' };

                Object selectedValue = JOptionPane.showInputDialog(null,
                        "Choose one", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null, 
                        possibleValues, possibleValues[0]);
                try {
                    if (selectedValue.equals('A')) {
                        int x;
                        if (p1.setA) {
                            x = JOptionPane.showConfirmDialog(null,
                                    "A is already set do"
                                    + " you want to overwrite it? ");
                        } else {
                            x = 0;
                        }
                        if (x == 0) {
                            String input = new String();
                            input = JOptionPane.showInputDialog(null,
                                    "enter A as (1,2),"
                                    + "(3,4),(5,6) without spaces"
                                    + "(powers are in decreasing order)");
                            if (input != null) {
                                ArrayList<Integer> buffer = new ArrayList<>();
                                p1.clearPolynomial('A');
                                input = input.replace(')', '#');
                                input = input.replace(',', '#');
                                input = input.replace('(', '#');
                                input = input.replace(' ', '#');
                                for (int i = 0; i < input.length(); i++) {
                                    if (input.charAt(i) == '#' 
                                      || input.charAt(i) == '-'
                                      || Character.isDigit(input.charAt(i))) {
                                        continue;
                                    } else {
                                        throw new RuntimeException();
                                    }
                                }
                                for (int i = 0; i < input.length(); i++) {
                                    while (i < input.length() 
                                            && input.charAt(i) == '#') {
                                        i++;
                                    }
                                    if (i < input.length() 
                                            && input.charAt(i) == '-') {
                                        i++;
                                      StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()) * -1);
                                    } else if (i < input.length()) {
                                        StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()));
                                    }
                                }
                                int[][] a = new int[buffer.size() / 2][2];
                                for (int j = 0; j < buffer.size(); j++) {
                                    a[j / 2][j % 2] = buffer.get(j);
                                }
                                p1.setPolynomial('A', a);
                                JOptionPane.showMessageDialog(null, "A is set");
                                buffer.clear();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "A is not set");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "A is not set");
                        }
                    } else if (selectedValue.equals('B')) {
                        int x;
                        if (p1.setB) {
                            x = JOptionPane.showConfirmDialog(null,
                                    "B is already set do you"
                                    + " want to overwrite it? ");
                        } else {
                            x = 0;
                        }
                        if (x == 0) {
                            String input = new String();
                            input = JOptionPane.showInputDialog(null,
                                    "enter B as (1,2),(3,4),(5,6) without" 
                            + "spaces(powers are in decreasing order)");
                            if (input != null) {
                                ArrayList<Integer> buffer = new ArrayList<>();
                                p1.clearPolynomial('B');
                                input = input.replace(')', '#');
                                input = input.replace(',', '#');
                                input = input.replace('(', '#');
                                input = input.replace(' ', '#');
                                for (int i = 0; i < input.length(); i++) {
                                    if (input.charAt(i) == '#' 
                                       || input.charAt(i) == '-'
                                       || Character.isDigit(input.charAt(i))) {
                                        continue;
                                    } else {
                                        throw new RuntimeException();
                                    }
                                }
                                for (int i = 0; i < input.length(); i++) {
                                    while (i < input.length() 
                                            && input.charAt(i) == '#') {
                                        i++;
                                    }
                                    if (i < input.length() 
                                            && input.charAt(i) == '-') {
                                        i++;
                                        StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()) * -1);
                                    } else if (i < input.length()) {
                                        StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()));
                                    }
                                }
                                int[][] a = new int[buffer.size() / 2][2];
                                for (int j = 0; j < buffer.size(); j++) {
                                    a[j / 2][j % 2] = buffer.get(j);
                                }
                                p1.setPolynomial('B', a);
                                JOptionPane.showMessageDialog(null, "B is set");
                                buffer.clear();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "B is not set");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "B is not set");
                        }
                    } else if (selectedValue.equals('C')) {
                        int x;
                        if (p1.setC) {
                            x = JOptionPane.showConfirmDialog(null,
                                    "C is already set"
                                    + " do you want to overwrite it? ");
                        } else {
                            x = 0;
                        }
                        if (x == 0) {
                            String input = new String();
                            input = JOptionPane.showInputDialog(null,
                                    "enter C as "
                                    + "(1,4),(3,3),(5,2) "
                                    + "without spaces(powers are in decreasing"
                                    + " order)");
                            if (input != null) {
                                ArrayList<Integer> buffer = new ArrayList<>();
                                p1.clearPolynomial('C');
                                input = input.replace(')', '#');
                                input = input.replace(',', '#');
                                input = input.replace('(', '#');
                                input = input.replace(' ', '#');
                                for (int i = 0; i < input.length(); i++) {
                                    if (input.charAt(i) == '#' 
                                        || input.charAt(i) == '-'
                                        || Character.isDigit(input.charAt(i))) {
                                        continue;
                                    } else {
                                        throw new RuntimeException();
                                    }
                                }
                                for (int i = 0; i < input.length(); i++) {
                                    while (i < input.length() 
                                            && input.charAt(i) == '#') {
                                        i++;
                                    }
                                    if (i < input.length() 
                                            && input.charAt(i) == '-') {
                                        i++;
                                        StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()) * -1);
                                    } else if (i < input.length()) {
                                        StringBuilder temp = new StringBuilder();
                                        while (i < input.length() 
                                                && input.charAt(i) != '#') {
                                            temp.append(input.charAt(i));
                                            i++;
                                        }
                                        buffer.add(Integer.valueOf(temp.toString()));
                                    }
                                }
                                int[][] a = new int[buffer.size() / 2][2];
                                for (int j = 0; j < buffer.size(); j++) {
                                    a[j / 2][j % 2] = buffer.get(j);
                                }
                                p1.setPolynomial('C', a);
                                JOptionPane.showMessageDialog(null, "C is set");
                                buffer.clear();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "C is not set");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "C is not set");
                        }
                    }
                } catch (RuntimeException e) {
                    JOptionPane.showMessageDialog(null, " nothing is set ! ");
                }
            }
        });
        btnNewButton.setBackground(new Color(255, 255, 0));
        btnNewButton.setBounds(105, 316, 97, 43);
        frame.getContentPane().add(btnNewButton);
        ///////////////////////////////////////////////////////////////////
        JButton btnNewButton_1 = new JButton("2");
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent click) {
                Object[] possibleValues = { 'A', 'B', 'C', 'R' };
                Object selectedValue = JOptionPane.showInputDialog(null,
                        "Choose one", "Input",
                        JOptionPane.INFORMATION_MESSAGE,
                        null, possibleValues,
                        possibleValues[0]);
                if (selectedValue.equals('A')) {
                    if (!p1.setA) {
                        JOptionPane.showMessageDialog(null,
                                "A has not been set yet ");
                    } else {
                        String out = p1.print('A');
                        JOptionPane.showMessageDialog(null, out);
                    }
                } else if (selectedValue.equals('B')) {
                    if (!p1.setB) {
                        JOptionPane.showMessageDialog(null,
                                "B has not been set yet ");
                    } else {
                        String out = p1.print('B');
                        JOptionPane.showMessageDialog(null, out);
                    }
                } else if (selectedValue.equals('C')) {
                    if (!p1.setC) {
                        JOptionPane.showMessageDialog(null,
                                "C has not been set yet ");
                    } else {
                        String out = p1.print('C');
                        JOptionPane.showMessageDialog(null, out);
                    }
                } else if (selectedValue.equals('R')) {
                    if (!p1.setR) {
                        JOptionPane.showMessageDialog(null,
                                "R has not been set yet ");
                    } else {
                        String out = p1.print('R');
                        JOptionPane.showMessageDialog(null, out);
                    }
                }
            }
        });
        btnNewButton_1.setBackground(new Color(255, 255, 0));
        btnNewButton_1.setBounds(261, 316, 97, 43);
        frame.getContentPane().add(btnNewButton_1);
        /////////////////////////////////////////
        JButton btnNewButton_2 = new JButton("3");
        btnNewButton_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent arg0) {
                Object[] possibleValues1 = { 'A', 'B', 'C', 'R' };
                Object selectedValue1 = JOptionPane.showInputDialog(null,
                        "Choose first", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues1, possibleValues1[0]);
                Object[] possibleValues2 = { 'A', 'B', 'C', 'R' };
                Object selectedValue2 = JOptionPane.showInputDialog(null,
                        "Choose second", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues2, possibleValues2[0]);
                try {
                    p1.add((char) selectedValue1, (char) selectedValue2);
                    JOptionPane.showMessageDialog(null, p1.print('R'));
                } catch (RuntimeException e) {
                    JOptionPane.showMessageDialog(null,
                            "sorry you have chosen an empty polinomial!");
                }
            }
        });
        btnNewButton_2.setBackground(new Color(255, 255, 0));
        btnNewButton_2.setBounds(408, 316, 97, 43);
        frame.getContentPane().add(btnNewButton_2);
        /////////////////////////////////////////////////////////////////////
        JButton btnNewButton_3 = new JButton("4");
        btnNewButton_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Object[] possibleValues1 = { 'A', 'B', 'C', 'R' };
                Object selectedValue1 = JOptionPane.showInputDialog(null,
                        "Choose first", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues1, possibleValues1[0]);
                Object[] possibleValues2 = { 'A', 'B', 'C', 'R' };
                Object selectedValue2 = JOptionPane.showInputDialog(null,
                        "Choose second", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues2, possibleValues2[0]);
                try {
                    p1.subtract((char) selectedValue1, (char) selectedValue2);
                    JOptionPane.showMessageDialog(null, p1.print('R'));
                } catch (RuntimeException e1) {
                    JOptionPane.showMessageDialog(null,
                            "sorry you have chosen an empty polinomial!");
                }
            }
        });
        btnNewButton_3.setBackground(new Color(255, 255, 0));
        btnNewButton_3.setBounds(559, 316, 97, 43);
        frame.getContentPane().add(btnNewButton_3);
        /////////////////////////////////////////////////////////////////////
        JButton btnNewButton_4 = new JButton("5");
        btnNewButton_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Object[] possibleValues1 = { 'A', 'B', 'C', 'R' };
                Object selectedValue1 = JOptionPane.showInputDialog(null,
                        "Choose first", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues1, possibleValues1[0]);
                Object[] possibleValues2 = { 'A', 'B', 'C', 'R' };
                Object selectedValue2 = JOptionPane.showInputDialog(null,
                        "Choose second", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues2, possibleValues2[0]);
                try {
                    p1.multiply((char) selectedValue1, (char) selectedValue2);
                    JOptionPane.showMessageDialog(null, p1.print('R'));
                } catch (RuntimeException e2) {
                    JOptionPane.showMessageDialog(null,
                            "sorry you have chosen an empty polinomial!");
                }
            }
        });
        btnNewButton_4.setBackground(new Color(255, 255, 0));
        btnNewButton_4.setBounds(712, 316, 97, 43);
        frame.getContentPane().add(btnNewButton_4);
        //////////////////////////////////////////////////
        JButton btnNewButton_5 = new JButton("6");
        btnNewButton_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                Object[] possibleValues1 = { 'A', 'B', 'C', 'R' };
                Object selectedValue1 = JOptionPane.showInputDialog(null,
                        "Choose first", "Input",
                        JOptionPane.INFORMATION_MESSAGE, null,
                        possibleValues1, possibleValues1[0]);
                try {
                  String value = JOptionPane.showInputDialog("enter a value");
                  float res = p1.evaluatePolynomial((Character) selectedValue1,
                          Float.valueOf(value));
                  JOptionPane.showConfirmDialog(null,
                          "result = " + String.valueOf(res));
                } catch (RuntimeException e1) {
                    JOptionPane.showMessageDialog(null,
                            "selected polynomial is empty!");
                }
            }
        });
        btnNewButton_5.setBackground(new Color(255, 255, 0));
        btnNewButton_5.setBounds(871, 316, 97, 43);
        frame.getContentPane().add(btnNewButton_5);
        /////////////////////////////////////////////////////
    }
}
