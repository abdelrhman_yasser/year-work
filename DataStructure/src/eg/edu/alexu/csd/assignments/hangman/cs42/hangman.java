package eg.edu.alexu.csd.assignments.hangman.cs42;

import java.util.ArrayList;
import java.util.Random;

public class hangman implements eg.edu.alexu.csd.assignments.hangman.IHangman {

    public ArrayList<String> dictionary = new ArrayList<>();
    public int max_tries = 0;
    public int count = 1;
    public String ranword;
    public String copy = ranword;
    public String guessword = "";
    public StringBuilder sb = new StringBuilder(guessword);

    @Override
    public final void setDictionary(final String[] words) {
        for (int i = 0; i < words.length; i++) {
            dictionary.add(words[i]);
        }
    }

    @Override
    public final String selectRandomSecretWord() {
        int ran = 0;
        Random random = new Random();
        if (dictionary.size() > 0) {
            int temp = dictionary.size();
            ran = random.nextInt(temp);
            ranword = dictionary.get(ran);
            while (ranword.isEmpty()) {
                ranword = dictionary.get(ran);
            }
        } else {
            return null;
        }
        copy = ranword;
        for (int i = 0; i < ranword.length(); i++) {
            sb.append("-");
        }
        return dictionary.get(ran);
    }

    @Override
    public final String guess(final Character c) {
        // && !ret.contains(Character.toString(Character.toUpperCase(c)))
        // String ret = sb.toString();
        if (!ranword.toUpperCase().contains(Character.toString(Character.toUpperCase(c)))) {
            max_tries--;
        }
        if (max_tries > 0 && !ranword.isEmpty() && sb.toString().contains("-")) {
            for (int i = 0; i < (int) ranword.length(); i++) {
                char temp = ranword.charAt(i);
                if (temp == Character.toLowerCase(c) || temp == Character.toUpperCase(c)) {
                    sb.setCharAt(i, ranword.charAt(i));

                }
            }
            return sb.toString();
        } else if (max_tries > 0 && ranword.isEmpty()) {
            return sb.toString();
        } else {
            return null;
        }
    }

    @Override
    public final void setMaxWrongGuesses(final Integer max) {
        max_tries = max;
    }
}
