//package eg.edu.alexu.csd.datastructure.queue.cs42;
//
//import static org.junit.Assert.*;
//
//import org.junit.Test;
//
//import eg.edu.alexu.csd.datastructure.queue.IQueue;
//
//public class testqueues {
//
//    @Test
//    public void test() {
//        IQueue x = new Aqueue(5);
//        try {
//            x.dequeue();
//        } catch (Exception e) {
//           String mes = "empty";
//           assertEquals(mes,e.getMessage());
//        }
//        assertEquals(true, x.isEmpty());
//        assertEquals(0, x.size());
//        x.enqueue(3);
//        assertEquals(1, x.size());
//        x.enqueue(4);
//        //System.out.println(x.size());
//        assertEquals(2, x.size());
//        x.enqueue(5);
//        assertEquals(3, x.size());
//        //x.queue.printall();
//        //System.out.println(x.size());
//        assertEquals(3, x.dequeue());
//        assertEquals(2, x.size());
//        //x.queue.printall();
//        assertEquals(4, x.dequeue());
//        assertEquals(1, x.size());
//        //x.queue.printall();
//        
//        assertEquals(5, x.dequeue());      
//        assertEquals(0, x.size());
//        try {
//            x.dequeue();
//        } catch (Exception e) {
//           String mes = "empty";
//           assertEquals(mes,e.getMessage());
//        }
//        x.enqueue(3);
//        x.enqueue(4);
//        x.enqueue(5);
//        x.enqueue(3);
//        x.enqueue(4);
//        x.dequeue();
//        x.dequeue();
//        x.dequeue();
//        x.dequeue();
//        x.enqueue(4);
//        assertEquals(4, x.dequeue());
//        assertEquals(4, x.dequeue());
//        try {
//            x.dequeue();
//        } catch (Exception e) {
//           String mes = "empty";
//           assertEquals(mes,e.getMessage());
//        }
//        assertEquals(0, x.size());
//        assertEquals(true, x.isEmpty());
//        x.enqueue(1);
//        x.enqueue(2);
//        x.enqueue(3);
//        x.enqueue(4);
//        assertEquals(4, x.size());
//        x.dequeue();
//        x.dequeue();
//        x.dequeue();
//        assertEquals(1, x.size());
//        assertEquals(false, x.isEmpty());
//        x.dequeue();
//        x.enqueue(1);
//        x.enqueue(2);
//        x.enqueue(3);
//        x.enqueue(4);
//        x.enqueue(5);
//        assertEquals(5, x.size());
//    }
//
//    
//    @Test
//    public void test_2(){
//        Lqueue x = new Lqueue();
//        try {
//            x.dequeue();
//        } catch (Exception e) {
//           String mes = "";
//           assertEquals(mes,e.getMessage());
//        }
//        assertEquals(true, x.isEmpty());
//        assertEquals(0, x.size());
//        x.enqueue(3);
//        assertEquals(1, x.size());
//        x.enqueue(4);
//        //System.out.println(x.size());
//        assertEquals(2, x.size());
//        x.enqueue(5);
//        assertEquals(3, x.size());
//        //x.queue.printall();
//        //System.out.println(x.size());
//        assertEquals(3, x.dequeue());
//        assertEquals(2, x.size());
//        //x.queue.printall();
//        assertEquals(4, x.dequeue());
//        assertEquals(1, x.size());
//        //x.queue.printall();
//        
//        assertEquals(5, x.dequeue());      
//        assertEquals(0, x.size());
//        try {
//            x.dequeue();
//        } catch (Exception e) {
//           String mes = "";
//           assertEquals(mes,e.getMessage());
//        }
//        x.enqueue(3);
//        x.enqueue(4);
//        x.enqueue(5);
//        x.enqueue(3);
//        x.enqueue(4);
//        try {
//            x.enqueue(1);
//        } catch (Exception e) {
//            String mes = "";
//            assertEquals(mes,e.getMessage());
//        }
//    }
//
//}
