package RadixSort;

import java.util.ArrayList;

public class Radix {

	private static Radix inst;

	private Radix() {
	}

	public static Radix getInstance() {
		if (inst == null)
			inst = new Radix();
		return inst;
	}

	public void sort(Integer[] arr) {

		int radsize = 0;
		ArrayList<ArrayList<Integer>> rad = new ArrayList<>();
	
		for(int i=0 ; i<10 ; i++)rad.add(new ArrayList<Integer>());
		
		for (int i = 0; i < arr.length; i++) {
			rad.get(arr[i] % 10).add(arr[i]);
		}

		for (int i : arr)
			radsize = Math.max(radsize, (int) Math.ceil(Math.log10(i)));

		for (int i = 2; i <= radsize; i++) {
			rad = getRad(i, rad);
		}

		int coun = 0;
		for (int i = 0; i < rad.size(); i++) {
			for (int j = 0; j < rad.get(i).size(); j++) {
				arr[coun] = rad.get(i).get(j);
				coun++;
			}
		}

	}

	private ArrayList<ArrayList<Integer>> getRad(int x, ArrayList<ArrayList<Integer>> rad) {

		ArrayList<ArrayList<Integer>> ret = new ArrayList<>();
		for(int i=0 ; i<10 ; i++)
			ret.add(new ArrayList<Integer>());
		
		for (int i = 0; i < rad.size(); i++) {
			for (int j = 0; j < rad.get(i).size(); j++) {
				int num = rad.get(i).get(j), temp=num ; num /= (int) Math.pow(10, x-1) ;
				ret.get(num % 10).add(temp);
			}
		}
		return ret;
	}
	
	
	public static void main(String[] args) {
		
		Integer[] arr = {12 ,32 ,34  , 54 ,43 , 54, 45 , 245 , 123 ,54};
		Radix.getInstance().sort(arr);
		
		for(int i:arr)System.out.print(i + " ");
		
		
	}

}
