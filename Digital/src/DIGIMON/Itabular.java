package DIGIMON;

import java.util.ArrayList;
/**
 * 
 * @author DIGIMON
 *
 */
public interface Itabular {
    /**
     * turns binary number to its decimal form
     * 
     */
    public int turn_decimal(String bin);

    /**.
     * turn number to its binary sequence and return string of this sequence it
     * takes as parameters number to be converted , maximum number of divides by
     * 2 , index of number in sequence of impilicants
     */
    public String binary(int num, int maxdiv);

    /**.
     * return integer value of maximum divides by 2 of largest number in
     * sequence of implicants
     */
    public int get_max_repeat(int largenum);

    /**
     * return prime implicants of original implicants as form of vector
     * 
     */
    public ArrayList<String> prime_implicants(ArrayList<Integer> indeces, int maxdiv);

    /**.
     * get most simplified implicant it takes all prime implicants and find most
     * simplified using complete search
     */
    public void MSPE(int most , int begin, int index, ArrayList<ArrayList<Integer>> copy, boolean[] visited);

    /**. it hashes the table for findung most simplified implicant */
    public boolean complete(boolean[] visited, ArrayList<ArrayList<Integer>> copy);
}