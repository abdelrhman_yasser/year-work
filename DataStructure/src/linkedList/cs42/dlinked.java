package linkedList.cs42;

import linkedList.ILinkedList;
/**
 * 
 * @author abdo
 *
 */
public class dlinked implements linkedList.ILinkedList {

    private dlistnode head = new dlistnode();
    private dlistnode tail = new dlistnode();
    private int size;
    /**.
     * intialize the list
     */
    public dlinked() {
        this.size = 0;
        this.tail.setNext(null);
        this.tail.setPrev(head);
        this.head.setPrev(null);
        this.head.setNext(tail);
    }

    @Override
    public final void add(final int index, final Object element) {
        if (index < 0 || index > size) {
            throw new RuntimeException("");
        } else if (size == 0) {
            dlistnode newnode = new dlistnode();
            tail.setPrev(newnode);
            head.setNext(newnode);
            newnode.setPrev(head);
            newnode.setNext(tail);
            //newnode.setIndex(0);
            newnode.setItem(element);
            this.size++;
        }
        else if(index==size){
            dlistnode newnode = new dlistnode();
            newnode.setNext(tail);
            newnode.setPrev(tail.getPrev());
            tail.getPrev().setNext(newnode);
            tail.setPrev(newnode);
            newnode.setItem(element);
            this.size++;
        }
        else {
            dlistnode iterator = head;
            for (int i = 0; i < index; i++) {
                iterator = iterator.getNext();
            }
            dlistnode newnode = new dlistnode();
            newnode.setPrev(iterator);
            newnode.setNext(iterator.getNext());
            //newnode.setIndex(index);
            newnode.setItem(element);
            newnode.getNext().setPrev(newnode);
            iterator.setNext(newnode);
            /*dlistnode iterator2 = newnode;
            for (int i = index; i < size; i++) {
                iterator2 = iterator2.getNext();
                iterator2.setIndex(iterator2.getIndex() + 1);

            }*/
            this.size++;
        }
    }

    @Override
    public final void add(final Object element) {
        dlistnode newnode = new dlistnode();
        newnode.setItem(element);
        if (size == 0) {
            head.setNext(newnode);
            newnode.setPrev(head);
            newnode.setNext(tail);
            tail.setPrev(newnode);
            //newnode.setIndex(0);
            this.size++;
        } else {
            dlistnode iterator = head;
            for (int i = 0; i < size; i++) {
                iterator = iterator.getNext();
            }
            iterator.setNext(newnode);
            newnode.setPrev(iterator);
            newnode.setNext(tail);
            tail.setPrev(newnode);
            //newnode.setIndex(this.size);
            this.size++;
        }
    }

    @Override
    public final Object get(final int index) {
        dlistnode iterator = head;
        if (index > size - 1 || index < 0) {
            throw new RuntimeException("");
        }
        if (index == 0) {
            return head.getNext().getItem();
        } else {
            for (int i = 0; i < index + 1; i++) {
                iterator = iterator.getNext();
            }
            return iterator.getItem();
        }
    }

    @Override
    public final void set(final int index, final Object element) {
        dlistnode iterator = head;
        if (index > size - 1 || index < 0) {
            throw new RuntimeException("");
        }
        if (index == 0) {
            head.getNext().setItem(element);
        } else {
            for (int i = 0; i < index + 1; i++) {
                iterator = iterator.getNext();
            }
            iterator.setItem(element);
        }
    }

    @Override
    public final void clear() {
        dlistnode iterator = head;
        iterator.getNext();
        for (int i = 0; i <= size; i++) {
            dlistnode remove = iterator;
            iterator = iterator.getNext();
            remove.setNext(null);
        }
        this.size = 0;
    }
    /**.
     * print all of list 
     */
    public final void printall() {
        dlistnode iterator = head;
        for (int i = 0; i < size; i ++) {
            iterator = iterator.getNext();
            System.out.println("element--->" + iterator.getItem() 
            );
        }
    }

    @Override
    public final boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public final void remove(final int index) {
        if (index > size - 1 || index < 0 || size == 0) {
            throw new RuntimeException("");
        }
        if (index == size - 1) {
            dlistnode remove = tail.getPrev();
            remove.getPrev().setNext(tail);
            tail.setPrev(remove.getPrev());
            remove = null;
        } else if (index == 0) {
            dlistnode remove = head.getNext();
            head.setNext(head.getNext().getNext());
            remove.getNext().setPrev(head);
            remove = null;
            /*for (int i = 0; i < size; i++) {
                iterator = iterator.getNext();
                iterator.setIndex(iterator.getIndex() - 1);
            }*/
        } else {
            dlistnode iterator = head;
            for (int i = 0; i < index; i++) {
                iterator = iterator.getNext();
            }
            dlistnode remove = iterator.getNext();
            dlistnode iterator2 = remove;
            for (int i = index; i < size; i++) {
                iterator2 = iterator2.getNext();
                //iterator2.setIndex(iterator2.getIndex() - 1);
            }
            iterator.setNext(iterator.getNext().getNext());
            iterator.getNext().getNext().setPrev(iterator);
            remove = null;
        }
        this.size--;

    }

    @Override
    public final int size() {
        return this.size;
    }
    

    @Override
    public final ILinkedList sublist(final int fromIndex, final int toIndex) {
        dlinked newlist = new dlinked();
        dlistnode iterator = head;
        int i=0;
        iterator = iterator.getNext();
        if (fromIndex > size - 1 || fromIndex < 0 
            || toIndex > size - 1 || toIndex < 0 || toIndex < fromIndex) {
            throw new RuntimeException("");
        }
        for( i=0 ; i<fromIndex ; i++){
            iterator = iterator.getNext();            
        }
        newlist.add(iterator.getItem());
        for(int j=i ; i<toIndex ; i++){
            iterator = iterator.getNext();            
            newlist.add(iterator.getItem());
        }
        /*while (iterator.getIndex() < fromIndex) {
            iterator = iterator.getNext();
        }
        newlist.add(iterator.getItem());
        while (iterator.getIndex() < toIndex) {
            iterator = iterator.getNext();
            newlist.add(iterator.getItem());
        }*/
        return newlist;
    }

    @Override
    public final boolean contains(final Object o) {
        boolean found = false;
        dlistnode iterator = head;
        for (int i = 0; i < size; i++) {
            iterator = iterator.getNext();
            if (iterator.getItem().equals(o)) {
                found = true;
                i = size + 1;
            }
        }
        return found;
    }

}