package queue.cs42;

import queue.IArrayBased;
import queue.IQueue;

/**
 * 
 * @author abdo
 *
 */
public class Aqueue implements IQueue, IArrayBased {
    /**
     * . intializiing the array
     */
    private  int counsize=0;
    private  int size ;
    private Object x [];
    
    public Aqueue(int any){
        this.size=any;
        x = new Object[this.size];        
    }
    
    /**
     * . pointer to beg of queue
     */
    private int front = -1;
    /**
     * . pointer to end of queue
     */
    private int back = -1;

    @Override
    public final void enqueue(final Object item) {
        // TODO it insert new object in queue from back
        if ((front == back && front != -1 && back != -1) 
                || (front == -1 && back == this.size - 1)) {
            throw new RuntimeException("full");
        }
        back = (back + 1) % this.size;
        x[back] = item;
        this.counsize++;
    }

    @Override
    public final Object dequeue() {
        // TODO get first of the queue
        if (isEmpty()) {
            throw new RuntimeException("empty");
        }
        front = (front + 1) % this.size;
        Object ret = x[front];
        if (front == back) {
            front = -1;
            back = -1;
        }
        this.counsize--;
        return ret;
    }

    @Override
    public final boolean isEmpty() {
        // TODO check if is empty
        if (front == back && back == -1) {
            return true;
        }
        return false;
    }

    @Override
    public final int size() {
        // TODO return size of the array
        return counsize;
    }
}
