package linkedList.cs42;
/**
 * 
 * @author abdo
 *
 */
public class slistnode {
    private Object item;
    // private int index;
    private slistnode next;
    /**
     * 
     * @return ITEM OF NODE
     */
    public final Object getItem() {
        return this.item;
    }
    /**
     * 
     * @param item set item of node
     */
    public final void setItem(final Object item) {
        this.item = item;
    }
    /**
     * 
     * @return next node
      */
    public final slistnode getNext() {
        return this.next;
    }
    /**
     * 
     * @param next set next node
     */
    public final void setNext(slistnode next) {
        this.next = next;
    }
}
