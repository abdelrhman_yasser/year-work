//package eg.edu.alexu.csd.datastructure.stack.cs42;
//
//import static org.junit.Assert.*;
//
//import org.junit.Test;
//
//public class testevaluate {
//
//    @Test
//    public void MyStack() {
//        MyStack x = new MyStack();
//        x.push(4);
//        assertEquals(4, x.peek());
//        x.push(5);
//        assertEquals(5, x.peek());
//        x.push(6);
//        x.add(2, 7);
//        assertEquals(6, x.peek());
//        assertEquals(6, x.pop());
//        assertEquals(5, x.pop());
//        assertEquals(7, x.pop());
//        assertEquals(4, x.pop());
//        assertEquals(true, x.isEmpty());
//        assertEquals(0, x.size());
//    }
//    /**.
//     * test1
//     */
//    @Test
//    public final void testInfixToPostfix1() {
//        Evaluator x = new Evaluator();
//        assertEquals("12 13 24 35 * + * 12 +", x.infixToPostfix("12*(13+24*35)+12"));
//    }
//    /**.
//     * test2 
//     */
//    @Test
//    public final void testInfixToPostfix2() {
//        Evaluator x = new Evaluator();
//        System.out.print(x.infixToPostfix("55*(55+23-390/100)/123"));
//        assertEquals("55 55 23 + 390 100 / - * 123 /", x.infixToPostfix("55*(55+23-390/100)/123"));
//    }
//    /**.
//     * test3
//     */
//    @Test
//    public final void testInfixToPostfix3() {
//         Evaluator x = new Evaluator();
//         assertEquals("55 55 23 + 390 100 / � * 123 /", x.infixToPostfix("10 *  4 + (2 � 1/8) "));
//    }
//
//}
