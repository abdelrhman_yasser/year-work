package linkedList;

public interface ILinkedList {
    /**
     * @param index index in list
     * @param element object of list
     *            Inserts a specified element at the specified sposition in the
     *            list.
     */
    public void add(int index, Object element);

    /**
     * @param element this element to be added
     *  Inserts the specified element at the end of the list. */
    public void add(Object element);

    /**
     * @param index index in list
     * @return object Returns the element at the specified position in this
     *         list.
     */
    public Object get(int index);

    /**
     * @param index index in list
     * @param element object to be added
     *            Replaces the element at the specified position in this list
     *            with the specified element.,/.
     */
    public void set(int index, Object element);

    /** Removes all of the elements from this list. */
    public void clear();

    /**
     * @return true or false.
     *  Returns true if this list contains no elements. */
    public boolean isEmpty();

    /**
     * @param index
     *  Removes the element at the specified position in this list. */
    public void remove(int index);

    /** 
     * @return size of list
     * Returns the number of elements in this list. */
    public int size();

    /**
     *@param fromIndex begin index
     *@param toIndex end index
     *@return list
     *Returns a view of the portion of this list between the specified
     * fromIndex and toIndex, inclusively.
     */
    public ILinkedList sublist(int fromIndex, int toIndex);

    /**
     * @param o object
     * @return false or true 
     * Returns true if this list contains an element with the same value as the
     * specified element.
     */
    public boolean contains(Object o);
}
