package AVL;

public class avlTree<T extends Comparable<T>> implements IAVLTree{

	Node root ;
	
	public avlTree() {
	}
	
	@Override
	public void insert(Comparable key) {
		if(root==null)
			root = new Node<>(key) ;
		else
			root.insert(key);
	}

	@Override
	public boolean delete(Comparable key) {
		boolean deleted = root.delete(key); 
		return deleted;
	}

	@Override
	public boolean search(Comparable key) {
		return root.exists(key);
	}

	@Override
	public int height() {
		return root.getHieght();
	}

	@Override
	public INode getTree() {
		return root;
	}
	
}
