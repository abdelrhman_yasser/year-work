//package eg.edu.alexu.csd.datastructure.linkedList.cs42;
//
//import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;
///**
// * 
// * @author abdo
// *
// */
//public class Polsolver {
///**.
// * tes1 1
// */
//    @org.junit.Test
//    public final void testAdd() {
//        int mn = (10);
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < mn; i++) {
//            y.add(i + 1);
//        }
//        assertEquals(mn, y.size());
//        assertEquals(1, y.get(0));
//        assertEquals(mn, y.get(9));
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testAddIndex() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        y.add(2, 345);
//        assertEquals(2, y.get(1));
//        assertEquals(345, y.get(2));
//        assertEquals(3, y.get(3));
//        assertEquals(11, y.size());
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testAddIndex2() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        y.add(0, 345);
//        assertEquals(345, y.get(0));
//        assertEquals(1, y.get(1));
//        assertEquals(10, y.get(10));
//        assertEquals(11, y.size());
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testRemove() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        y.remove(0);
//        assertEquals(9, y.size());
//        assertEquals(2, y.get(0));
//        assertEquals(10, y.get(8));
//        y.remove(8);
//        assertEquals(8, y.size());
//        assertEquals(2, y.get(0));
//        assertEquals(9, y.get(7));
//        y.remove(0);
//        assertEquals(7, y.size());
//        assertEquals(3, y.get(0));
//        assertEquals(9, y.get(6));
//        y.remove(3);
//        assertEquals(6, y.size());
//        assertEquals(5, y.get(2));
//        assertEquals(7, y.get(3));
//        assertEquals(8, y.get(4));
//        assertEquals(9, y.get(5));
//        y.add(2, 8);
//        assertEquals(7, y.size());
//        assertEquals(8, y.get(2));
//        assertEquals(5, y.get(3));
//        assertEquals(7, y.get(4));
//        assertEquals(8, y.get(5));
//
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testSet() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        y.set(0, 45);
//        y.set(9, 54);
//        assertEquals(45, y.get(0));
//        assertEquals(54, y.get(9));
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testClearIsEmpty() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        y.clear();
//        assertEquals(true, y.isEmpty());
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testContains() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        assertEquals(true, y.contains(10));
//        assertEquals(true, y.contains(5));
//        assertEquals(false, y.contains(0));
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testSubList() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        ILinkedList z = y.sublist(0, 5);
//        assertEquals(6, z.size());
//        assertEquals(1, z.get(0));
//        assertEquals(6, z.get(5));
//        z.clear();
//        z = y.sublist(3, 7);
//        assertEquals(5, z.size());
//        assertEquals(4, z.get(0));
//        assertEquals(8, z.get(4));
//    }
//
//    /**.
//     * tes1 1
//     */
//    @org.junit.Test
//    public final void testErrors() {
//        ILinkedList y = new Slinked();
//        for (int i = 0; i < 10; i++) {
//            y.add(i + 1);
//        }
//        ILinkedList z = y.sublist(0, 5);
//        assertEquals(6, z.size());
//        assertEquals(1, z.get(0));
//        assertEquals(6, z.get(5));
//
//        boolean thrown = false;
//        try {
//            z.get(6);
//        } catch (RuntimeException error) {
//            thrown = true;
//        }
//        assertTrue(thrown);
//    }
//    /**.
//     * test1
//     */
//    @org.junit.Test
//    public final void testErrors2() {
//        ILinkedList y = new dlinked();
//        assertFalse(y.contains(null));
//
//    }
//
//}
