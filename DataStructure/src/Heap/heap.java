package Heap;

import java.util.ArrayList;
import java.util.Random;

public class heap {

	private ArrayList<Integer> data = new ArrayList<>();
	
	public void add(int num) {
		data.add(num);
		goUp(data.size() - 1);
	}

	public int getMin() throws Exception {
		if (data.isEmpty())
			throw new Exception("Data is empty");
		else
			return data.get(0);
	}

	public void extractMin() throws Exception {
		if (data.isEmpty())
			throw new Exception("Data is empty");
		else{
			data.set(0, data.get(data.size() - 1));
			data.remove(data.size() - 1);
			goDown(0);
		}
	}

	public void build(ArrayList<Integer> arr) {
		data = new ArrayList<>(arr) ;
		for(int i=arr.size()/2 ; i>=0 ; i--)
			goDown(i);
//		for(int i=0 ; i<arr.size() ; i++)add(arr.get(i));
	}

	public void sort(ArrayList<Integer> vec, boolean increasing) throws Exception {
		heap mapy = new heap();
		for (int i = 0; i < vec.size(); i++) {
			if(!increasing)vec.set(i, vec.get(i)*-1);
		}
		mapy.build(vec);
		ArrayList<Integer> ret = new ArrayList<>();
		for (int i = 0; i < vec.size(); i++) {
			if (increasing)
				ret.add(mapy.getMin());
			else
				ret.add(-mapy.getMin());
			mapy.extractMin();
		}
		for(int i=0; i<ret.size() ; i++)vec.set(i, ret.get(i));
	}

	private int left(int node) {
		if (2 * node + 1 >= data.size())
			return -1;
		else
			return 2 * node + 1;
	}

	private int right(int node) {
		if (2 * node + 2 >= data.size())
			return -1;
		else
			return 2 * node + 2;
	}

	private int log2(double num){
		return (int)Math.ceil((Math.log10(num)/Math.log10(2)));
	}
	
	private int parent(int node) {
		if (node == 0)
			return -1;
		else
			return (node - 1) / 2;
	}

	private void goUp(int node) {
		if (parent(node) == -1 || data.get(parent(node)) <= data.get(node))
			return;
		int temp = data.get(node);
		data.set(node, data.get(parent(node)));
		data.set(parent(node), temp);
		goUp(parent(node));
	}

	private void goDown(int node) {
		int swapped = left(node);
		if (swapped == -1)
			return;
		if (right(node) != -1 && data.get(right(node)) < data.get(swapped))
			swapped = right(node);
		if (data.get(swapped) < data.get(node)) {
			int temp = data.get(node);
			data.set(node, data.get(swapped));
			data.set(swapped, temp);
			goDown(swapped);
		}
	}
	
	public void print(){
		int levels = log2(data.size());
		for(int i=0 ; i<=levels ; i++){
			for(int j=0 ; j<levels-i ; j++)System.out.print("\t");
			for(int j=0 ; j<Math.pow(2, i) && ((int)Math.pow(2, i)+j-1)<data.size() ; j++)System.out.print(data.get((int)Math.pow(2, i)+j-1)+"\t");
			for(int j=0 ; j<levels-i ; j++)System.out.print("\t");
			System.out.println();
		}
	}
	
	
	private void takeIn(){
		System.out.println("Array to be sorted is \n");
		
	}
	
	public static void main(String args[]) throws Exception {
//		System.out.println("Array to be sorted is \n");
		heap mapy = new heap();
		Random rand = new Random();
		ArrayList<Integer> seq = new ArrayList<>();
		for(int j=0 ; j<10000000 ; j++){
			seq.add(rand.nextInt(100000)-rand.nextInt(100000));
//			System.out.print(seq.get(j)+" ");
		}
//		System.out.println("\n");
		double time = System.currentTimeMillis();
		mapy.sort(seq, true);
		System.out.println("sort took "+((System.currentTimeMillis()-time)/1000) + " sec\n");
//		System.out.println("Array sorted is : \n");
//		for(int i=0; i<seq.size() ; i++){System.out.print(seq.get(i)+" ");}
		//----------------------------------
//		double time = System.currentTimeMillis();
//		mapy.build(seq);
//		System.out.println("building took "+(System.currentTimeMillis()-time)/1000 + " sec");
//		for(int i=0; i<seq.size() ; i++){
//			System.out.print(mapy.getMin()+" ");
//			time = System.currentTimeMillis();
//			mapy.extractMin();
//			System.out.println("building took "+(System.currentTimeMillis()-time)/1000 + " sec");
//		}
		//----------------------------------
	}
}
