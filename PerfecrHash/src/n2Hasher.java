import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class n2Hasher {
	
	private static n2Hasher instance;
	private ArrayList<Integer> rnums = new ArrayList<>();
	private int max = 0 , p , k ,size , rebuilds = 0 ;
	private FileOutputStream out ;
	private Integer[] hashMap ;
	private Random r = new Random();
	
	public static n2Hasher getInst() {
		if (instance == null)
			instance = new n2Hasher();
		return instance;
	}
	
	
	public void build(String path , String outPath){
		try {
			out = new FileOutputStream(new File(outPath));
			ReadNumbers(path);
			while(!tri())rebuilds++;
			print(out);
			out.close();
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void print(FileOutputStream f) throws IOException{
		String s = "K = " + k + " P = "+ p + " rebuilds = "+ rebuilds  +" Elements >> \n";
		f.write(s.getBytes());
		for(int i=0 ; i<hashMap.length ; i++){
			if(hashMap[i]!=null){
				s = i + " " ;
				s += " " + hashMap[i] +"\n";
				f.write(s.getBytes());
			}
		}
	}
	
	private boolean tri() throws Exception{
		size = (int) Math.pow(rnums.size(),2);
		hashMap = new Integer[size] ;
		boolean[] token = new boolean[size] ;
		
		p = PrimeGenerator.getInstance().getPrimeGreaterN(max);
		chooseK();
		
		for(int i : rnums){
			int key = (int)(((long)k%p*(long)i%p)%p)%size;
			if(token[key]){
				return false;
			}
			hashMap[key] = i;
			token[key] = true ;
		}
			
		return true;	
	}
	
	private void chooseK(){
		k = -1;
		while (k <= 0)
			k = r.nextInt(10000);
	}
	
	private void ReadNumbers(String filPath) throws IOException {
		File in = new File(filPath);

		FileInputStream reader = new FileInputStream(in);

		int n;
		StringBuilder snum = new StringBuilder();

		while ((n = reader.read()) != -1) {
			char c = (char) n;
			if (c == ',') {
				int number = Integer.valueOf(snum.toString());
				max = Math.max(number, max);
				if(!rnums.contains(number))rnums.add(number);
				snum = new StringBuilder();
			}else
				snum.append(c);
		}
		
		reader.close();
	}
	
	
}
