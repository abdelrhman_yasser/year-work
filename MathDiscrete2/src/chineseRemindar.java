import java.util.ArrayList;

public class chineseRemindar {

    int M;
    ArrayList<Integer> mi = new ArrayList<>();
    
    public chineseRemindar(int M) {
        this.M=M;
        Integer n = new Integer(M);
        getFactors(n);
    }
    
    private void getFactors(int M){
        if (M == 0 || M == 1)
            return;
        for (int i=2; i*i <= M; i++)
        {
            while (M%i == 0)
            {
                mi.add(i);
                M/=i;
            }
        }
        if (M > 1)
            mi.add(M);
    }
    
    private ArrayList<Integer> getAi (int A){
        ArrayList<Integer> ai =new ArrayList<>();
        for(int i=0 ; i<mi.size() ; i++){
            ai.add(A%mi.get(i));
        }
        return ai;
    }
    
    private Integer getA (ArrayList<Integer> ai){
        Integer A=0;
        for(int i=0 ; i<mi.size() ; i++){
            int Mi = M/mi.get(i);
            A += Mi*(int)(Math.pow(Mi, (mi.get(i)-2)))*ai.get(i);
        }
        return A%M;
    }
    
    public Integer addMOdM(Integer A ,Integer B){
        ArrayList<Integer> ai = getAi(A);
        ArrayList<Integer> bi = getAi(B);
        ArrayList<Integer> res = adda(ai, bi);
        return getA(res);
    }
    
    private ArrayList<Integer> adda (ArrayList<Integer> A , ArrayList<Integer> B){
        ArrayList<Integer> res = new ArrayList<>();
        for(int i=0 ; i<A.size() ; i++){
            res.add(A.get(i)+B.get(i));
        }
        return res;
    }
    
    
    public Integer multiMOdM(Integer A ,Integer B){
        ArrayList<Integer> ai = getAi(A);
        ArrayList<Integer> bi = getAi(B);
        ArrayList<Integer> res = multia(ai, bi);
        return getA(res);
    }
    
    private ArrayList<Integer> multia (ArrayList<Integer> A , ArrayList<Integer> B){
        ArrayList<Integer> res = new ArrayList<>();
        for(int i=0 ; i<A.size() ; i++){
            res.add(A.get(i)*B.get(i));
        }
        return res;
    }
}
