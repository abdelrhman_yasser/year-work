package queue.cs42;

import linkedList.cs42.dlinked;
import queue.ILinkedBased;
import queue.IQueue;

/**
 * 
 * @author abdo
 *
 */
public class Lqueue implements IQueue, ILinkedBased {
   /**
    * intializing list. 
    */
   private dlinked queue = new dlinked();
    /**.
     * @param item pushed object i queue
     * add element to queue
     */
    public final void enqueue(final Object item) {
        queue.add(queue.size(), item);
    }

    /**.
     * @return object to be popped from front of queue
     * get last element in queue
     */
    public final Object dequeue() {
        Object ret = queue.get(0);
        queue.remove(0);
        return ret;
    }

    /**
     * @return true if empty and vice versa
     */
    public final boolean isEmpty() {
        return queue.isEmpty();
    }

    /**
     * @return size of queue
     */
    public final int size() {
        return queue.size();
    }
    
}
