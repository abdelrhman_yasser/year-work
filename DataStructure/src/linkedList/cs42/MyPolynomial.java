package linkedList.cs42;

import linkedList.ILinkedList;
import linkedList.IPolynomialSolver;
/**
 * 
 * @author abdo
 *
 */
public class MyPolynomial implements IPolynomialSolver {
    ILinkedList A = new Slinked();
    ILinkedList B = new Slinked();
    ILinkedList C = new Slinked();
    ILinkedList R = new Slinked();
    boolean setA = false;
    boolean setB = false;
    boolean setC = false;
    boolean setR = false;

    @Override
    public final void setPolynomial(final char poly, final int[][] terms) {
        int var = terms[0][1];
        for (int i = 1; i < terms.length; i++) {
            if (var <= terms[i][1] /* ta3dil */ || terms[i][0] == 0) {
                throw new RuntimeException();
            } else {
                var = terms[i][1];
            }
        }
        switch (poly) {
        case 'A':
            for (int i = 0; i < terms.length; i++) {
                if (terms[i][1] < 0) {
                    throw new RuntimeException();
                }
                PolynomialNode nodeA = new PolynomialNode();
                nodeA.set_exp(terms[i][1]);
                nodeA.set_coff(terms[i][0]);
                A.add(nodeA);
            }
            setA = true;
            break;
        case 'B':
            for (int i = 0; i < terms.length; i++) {
                if (terms[i][1] < 0) {
                    throw new RuntimeException();
                }
                PolynomialNode nodeB = new PolynomialNode();
                nodeB.set_exp(terms[i][1]);
                nodeB.set_coff(terms[i][0]);
                B.add(nodeB);
            }
            setB = true;
            break;
        case 'C':
            for (int i = 0; i < terms.length; i++) {
                if (terms[i][1] < 0) {
                    throw new RuntimeException();
                }
                PolynomialNode nodeC = new PolynomialNode();
                nodeC.set_exp(terms[i][1]);
                nodeC.set_coff(terms[i][0]);
                C.add(nodeC);
            }
            setC = true;
            break;
        default:
            throw new RuntimeException();
        }
    }

    @Override
    public final String print(final char poly) {
        ILinkedList X = new Slinked();
        switch (poly) {
        case 'A':
            if (!setA) {
                return null;
            }
            X = A;
            break;
        case 'B':
            if (!setB) {
                return null;
            }
            X = B;
            break;
        case 'C':
            if (!setC) {
                return null;
            }
            X = C;
            break;
        case 'R':
            if (!setR) {
                return null;
            }
            X = R;
            break;
        default:
            throw new RuntimeException();
        }
        String req = new String();
        PolynomialNode node = new PolynomialNode();
        for (int i = 0; i < X.size(); i++) {
            node = (PolynomialNode) X.get(i);
            if (node.get_exp() == 0) {
                if (node.get_coff() != 0) {
                    if (i != 0 && node.get_coff() > 0) {
                        req += "+" + node.get_coff();
                    } else {
                        req += node.get_coff();
                    }
                }
            } else if (node.get_exp() == 1) {
                if (node.get_coff() != 0) {
                    if (node.get_coff() == 1 && i == 0) {
                        req += "x";
                    } else if (node.get_coff() == 1) {
                        req += "+x";
                    } else if (node.get_coff() == -1) {
                        req += "-x";
                    } else if (i != 0 && node.get_coff() > 0) {
                        req += "+" + node.get_coff() + "x";
                    } else {
                        req += node.get_coff() + "x";
                    }
                }
            } else {
                if (node.get_coff() != 0) {
                    if (node.get_coff() == 1 && i == 0) {
                        req += "x^" + node.get_exp();
                    } else if (node.get_coff() == 1) {
                        req += "+x^" + node.get_exp();
                    } else if (node.get_coff() == -1) {
                        req += "-x^" + node.get_exp();
                    } else if (i != 0 && node.get_coff() > 0) {
                        req += "+" + node.get_coff() + "x^" + node.get_exp();
                    } else {
                        req += node.get_coff() + "x^" + node.get_exp();
                    }
                }
            }
        }
        if (req.length() == 0) {
            req += "0";
        }
        return req;
    }

    @Override
    public final void clearPolynomial(final char poly) {
        switch (poly) {
        case 'A':
            A.clear();
            setA = false;
            break;
        case 'B':
            B.clear();
            setB = false;
            break;
        case 'C':
            C.clear();
            setC = false;
            break;
        case 'R':
            R.clear();
            setR = false;
            break;
        default:
            throw new RuntimeException();
        }

    }

    @Override
    public final float evaluatePolynomial(final char poly, final float value) {
        float result = 0;
        switch (poly) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            PolynomialNode nodeA = new PolynomialNode();
            for (int i = 0; i < A.size(); i++) {
                nodeA = (PolynomialNode) A.get(i);
                result += nodeA.get_value(value);
            }
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            PolynomialNode nodeB = new PolynomialNode();
            for (int i = 0; i < B.size(); i++) {
                nodeB = (PolynomialNode) B.get(i);
                result += nodeB.get_value(value);
            }
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            PolynomialNode nodeC = new PolynomialNode();
            for (int i = 0; i < C.size(); i++) {
                nodeC = (PolynomialNode) C.get(i);
                result += nodeC.get_value(value);
            }
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            PolynomialNode nodeR = new PolynomialNode();
            for (int i = 0; i < R.size(); i++) {
                nodeR = (PolynomialNode) R.get(i);
                result += nodeR.get_value(value);
            }
            break;
        default:
            throw new RuntimeException();
        }
        return result;
    }
    @Override
    public final int[][] add(final char poly1, final char poly2) {
        ILinkedList X = new Slinked();
        ILinkedList Y = new Slinked();
        switch (poly1) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            X = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            X = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            X = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            X = R;
            break;
        default:
            throw new RuntimeException();
        }
        switch (poly2) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            Y = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            Y = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            Y = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            Y = R;
            break;
        default:
            throw new RuntimeException();
        }
        PolynomialNode nodeY = new PolynomialNode();
        PolynomialNode nodeX = new PolynomialNode();
        ILinkedList result = new Slinked();
        for (int i = 0; i < X.size(); i++) {
            result.add(X.get(i));
        }
        for (int j = 0; j < Y.size(); j++) {
            int flag = 0;
            nodeY = (PolynomialNode) Y.get(j);
            for (int i = 0; i < result.size(); i++) {
                PolynomialNode nodeZ = new PolynomialNode();
                nodeX = (PolynomialNode) result.get(i);
                if (nodeY.get_exp() == nodeX.get_exp()) {
                    nodeZ.set_coff(nodeY.get_coff() + nodeX.get_coff());
                    nodeZ.set_exp(nodeY.get_exp());
                    result.set(i, nodeZ);
                    flag = 1;
                    break;
                }
                if (nodeY.get_exp() > nodeX.get_exp()) {
                    result.add(i, nodeY);
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                result.add(nodeY);
            }
        }
        R.clear();
        for (int i = 0; i < result.size(); i++) {
            R.add(result.get(i));
        }
        setR = true;
        check(); /* ta3dil */
        int[][] req = new int[R.size()][2];
        PolynomialNode node = new PolynomialNode();
        for (int i = 0; i < R.size(); i++) {
            node = (PolynomialNode) R.get(i);
            req[i][0] = node.get_coff();
            req[i][1] = node.get_exp();
        }
        return req;
    }

    @Override
    public final int[][] subtract(final char poly1, final char poly2) {
        ILinkedList X = new Slinked();
        ILinkedList Y = new Slinked();
        switch (poly1) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            X = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            X = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            X = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            X = R;
            break;
        default:
            throw new RuntimeException();
        }
        switch (poly2) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            Y = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            Y = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            Y = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            Y = R;
            break;
        default:
            throw new RuntimeException();
        }
        PolynomialNode nodeY = new PolynomialNode();
        PolynomialNode nodeX = new PolynomialNode();
        ILinkedList result = new Slinked();
        for (int i = 0; i < X.size(); i++) {
            result.add(X.get(i));
        }
        for (int j = 0; j < Y.size(); j++) {
            int flag = 0;
            nodeY = (PolynomialNode) Y.get(j);
            for (int i = 0; i < result.size(); i++) {
                PolynomialNode nodeZ = new PolynomialNode();
                nodeX = (PolynomialNode) result.get(i);
                if (nodeY.get_exp() == nodeX.get_exp()) {
                    nodeZ.set_coff(nodeX.get_coff() - nodeY.get_coff());
                    nodeZ.set_exp(nodeY.get_exp());
                    result.set(i, nodeZ);
                    flag = 1;
                    break;
                }
                if (nodeY.get_exp() > nodeX.get_exp()) {
                    nodeZ.set_coff(-1 * nodeY.get_coff());
                    nodeZ.set_exp(nodeY.get_exp());
                    result.add(i, nodeZ);
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                PolynomialNode nodeZ = new PolynomialNode();
                nodeZ.set_coff(-1 * nodeY.get_coff());
                nodeZ.set_exp(nodeY.get_exp());
                result.add(nodeZ);
            }
        }
        R.clear();
        for (int i = 0; i < result.size(); i++) {
            R.add(result.get(i));
        }
        setR = true;
        check(); /* ta3dil */
        int[][] req = new int[R.size()][2];
        PolynomialNode node = new PolynomialNode();
        for (int i = 0; i < R.size(); i++) {
            node = (PolynomialNode) R.get(i);
            req[i][0] = node.get_coff();
            req[i][1] = node.get_exp();
        }
        return req;
    }

    public int[][] multiply(char poly1, char poly2) {
        ILinkedList X = new Slinked();
        ILinkedList Y = new Slinked();
        switch (poly1) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            X = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            X = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            X = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            X = R;
            break;
        default:
            throw new RuntimeException();
        }
        switch (poly2) {
        case 'A':
            if (!setA) {
                throw new RuntimeException();
            }
            Y = A;
            break;
        case 'B':
            if (!setB) {
                throw new RuntimeException();
            }
            Y = B;
            break;
        case 'C':
            if (!setC) {
                throw new RuntimeException();
            }
            Y = C;
            break;
        case 'R':
            if (!setR) {
                throw new RuntimeException();
            }
            Y = R;
            break;
        default:
            throw new RuntimeException();
        }
        PolynomialNode nodeY = new PolynomialNode();
        PolynomialNode nodeX = new PolynomialNode();
        PolynomialNode nodeZ = new PolynomialNode();
        ILinkedList result = new Slinked();
        for (int q = 0; q < X.size(); q++) {
            nodeY = (PolynomialNode) X.get(q);
            for (int w = 0; w < Y.size(); w++) {
                nodeZ = (PolynomialNode) Y.get(w);
                PolynomialNode nodeE = new PolynomialNode();
                nodeE.set_coff(nodeZ.get_coff() * nodeY.get_coff());
                nodeE.set_exp(nodeZ.get_exp() + nodeY.get_exp());
                int flag = 0;
                for (int i = 0; i < result.size(); i++) {
                    nodeX = (PolynomialNode) result.get(i);
                    if (nodeE.get_exp() == nodeX.get_exp()) {
                        nodeE.set_coff(nodeE.get_coff() + nodeX.get_coff());
                        result.set(i, nodeE);
                        flag = 1;
                        break;
                    }
                    if (nodeE.get_exp() > nodeX.get_exp()) {
                        result.add(i, nodeE);
                        flag = 1;
                        break;
                    }
                }
                if (flag == 0) {
                    result.add(nodeE);
                }
            }
        }
        R.clear();
        for (int i = 0; i < result.size(); i++) {
            R.add(result.get(i));
        }
        setR = true;
        check(); /* ta3dil */
        int[][] req = new int[R.size()][2];
        PolynomialNode node = new PolynomialNode();
        for (int i = 0; i < R.size(); i++) {
            node = (PolynomialNode) R.get(i);
            req[i][0] = node.get_coff();
            req[i][1] = node.get_exp();
        }
        return req;
    }
    /**.
     * check validity of input 
     */
    private void check() {
        for (int i = 0; i < R.size(); i++) {
            if (((PolynomialNode) R.get(i)).get_coff() == 0) {
                R.remove(i);
                i--;
            }
        }
        if (R.size() == 0) {
            PolynomialNode zer = new PolynomialNode();
            R.add(zer);
        }
    }

}
