package AVL;

public class AVLTree1 {

	private Integer data;
	private Integer coun = 0, hieght = 1;
	private AVLTree1 left;
	private AVLTree1 right;
	private AVLTree1 parent;

	private AVLTree1(AVLTree1 parent) {
		this.parent = parent;
	}

	public AVLTree1() {
	}

	private int blcFactor() {
		return (left == null ? 0 : left.hieght) - (right == null ? 0 : right.hieght);
	}

	private void leftRot(AVLTree1 node) {
		if (parent(node) == null) {
			int inside = node.data;
			node.data = node.right.data;
			AVLTree1 ne = new AVLTree1();
			ne.data = inside;
			if (node.left != null)
				node.left.parent = ne;
			ne.left = node.left;
			if (node.right.left != null)
				node.right.left.parent = ne;
			ne.right = node.right.left;
			AVLTree1 delete = node.right;
			node.right.right.parent = node;
			node.right = node.right.right;
			delete = null;
			node.left = ne;
			ne.parent = node;
			updateHieght(node);
			updateHieght(node.left);
			updateHieght(node.right);
		} else {
			AVLTree1 temp = node.right;
			temp.parent = node.parent;
			node.parent = temp;
			node.right = temp.left;
			if (temp.left != null)
				temp.left.parent = node;
			if (temp.parent.right != null && temp.parent.right.equals(node))
				temp.parent.right = temp;
			else
				temp.parent.left = temp;
			temp.left = node;
			updateHieght(node);
			updateHieght(temp);
		}
	}

	private void rightRot(AVLTree1 node) {
		if (parent(node) == null) {
			int inside = node.data;
			node.data = node.left.data;
			AVLTree1 ne = new AVLTree1();
			ne.data = inside;
			if (node.right != null)
				node.right.parent = ne;
			ne.right = node.right;
			if (node.left.right != null)
				node.left.right.parent = ne;
			ne.left = node.left.right;
			AVLTree1 delete = node.left;
			node.left.left.parent = node;
			node.left = node.left.left;
			delete = null;
			node.right = ne;
			ne.parent = node;
			updateHieght(node);
			updateHieght(node.left);
			updateHieght(node.right);
		} else {
			AVLTree1 temp = node.left;
			temp.parent = node.parent;
			node.parent = temp;
			node.left = temp.right;
			if (temp.right != null)
				temp.right.parent = node;
			if (temp.parent.left != null && temp.parent.left.equals(node))
				temp.parent.left = temp;
			else
				temp.parent.right = temp;
			temp.right = node;
			updateHieght(node);
			updateHieght(temp);
		}
	}

	private void remove() {
		if (this.parent.left != null && this.parent.left.equals(this))
			this.parent.left = null;
		else
			this.parent.right = null;
	}

	private void balance(AVLTree1 node) {
		int blc = node.blcFactor();
		if (blc == 2) {
			if (node.left.blcFactor() == -1)
				leftRot(node.left);
			rightRot(node);
		} else if (blc == -2) {
			if (node.right.blcFactor() == 1)
				rightRot(node.right);
			leftRot(node);
		}
	}

	public void insert(int num) {

		if (data == null) {
			data = num;
			coun++;
		} else if (num > data) {
			if (right == null)
				right = new AVLTree1(this);
			right.insert(num);
		} else if (num < data) {
			if (left == null)
				left = new AVLTree1(this);
			left.insert(num);
		} else
			coun++;

		updateHieght(this);
		balance(this);

	}

	private void updateHieght(AVLTree1 node) {
		node.hieght = 1
				+ Math.max((node.left == null ? 0 : node.left.hieght), (node.right == null ? 0 : node.right.hieght));
	}

	public void inTraverse() {
		if (left != null)
			left.inTraverse();
		System.out.print(data + " ");
		if (right != null)
			right.inTraverse();
	}

	public void preTraverse() {
		System.out.print(data + " ");
		if (left != null)
			left.preTraverse();
		if (right != null)
			right.preTraverse();
	}

	public void postTraverse() {
		if (left != null)
			left.postTraverse();
		if (right != null)
			right.postTraverse();
		System.out.print(data + " ");
	}

	public int getMin() {
		if (this.left == null)
			return data;
		else
			return left.getMin();
	}

	public int getMax() {
		if (this.right == null)
			return data;
		else
			return right.getMax();
	}

	// return null if num was not found
	private AVLTree1 search(int num) {
		if (data.equals(num))
			return this;
		else if (num > data) {
			if (right == null)
				return right;
			else
				return right.search(num);
		} else {
			if (left == null)
				return left;
			else
				return left.search(num);
		}
	}

	// key->num of successor of node null if there's no num->key for node
	public Integer successor(int num) {
		AVLTree1 numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.right != null)
			return numNode.right.getMin();
		else {
			int nodeData = numNode.data;
			numNode = parent(numNode);
			while (parent(numNode) != null && parent(parent(numNode)) != null
					&& !parent(numNode).equals(parent(parent(numNode)).left)) {
				numNode = parent(numNode);
			}
			if (parent(numNode) == null)
				return nodeData;
			return numNode.data;
		}
	}

	// key->num of successor of node null if there's no num->key for node
	public Integer predecessor(int num) {
		AVLTree1 numNode = search(num);
		if (numNode == null)
			return null;
		if (numNode.left != null)
			return numNode.left.getMax();
		else {
			int nodeData = numNode.data;
			while (parent(numNode) != null && !numNode.equals(parent(numNode).right)) {
				numNode = parent(numNode);
			}
			if (parent(numNode) == null)
				return nodeData;
			return numNode.data;
		}
	}

	private AVLTree1 parent(AVLTree1 node) {
		return node.parent;
	}

	private void updateDel(AVLTree1 toDelete) {
		while (parent(toDelete) != null) {
			updateHieght(toDelete);
			balance(toDelete);
			toDelete = parent(toDelete);
		}
		updateHieght(toDelete);
		balance(toDelete);
	}

	public Boolean delete(int num) {
		AVLTree1 up = null, toDelete = search(num);
		if (toDelete == null)
			return false;
		if (toDelete.left == null && toDelete.right == null) {
			up = parent(toDelete);
			toDelete.remove();
			toDelete = null;
		} else if (toDelete.left == null && toDelete.right != null) {
			if (parent(toDelete) == null) {
				toDelete.left = toDelete.right.left;
				toDelete.left.parent = toDelete;
				toDelete.right = toDelete.right.right;
				toDelete.data = toDelete.right.parent.data;
				toDelete.right.parent = null;
				toDelete.right.parent = toDelete;
				up = toDelete;
			} else {
				if (parent(toDelete).right.equals(toDelete)) {
					parent(toDelete).right = toDelete.right;
				} else {
					parent(toDelete).left = toDelete.right;
				}
				toDelete.right.parent = parent(toDelete);
				up = parent(toDelete);
				toDelete =null;
			}
		} else if (toDelete.left != null && toDelete.right == null) {
			if (parent(toDelete) == null) {
				toDelete.right = toDelete.left.right;
				toDelete.right.parent = toDelete;
				toDelete.left = toDelete.left.left;
				toDelete.data = toDelete.left.parent.data;
				toDelete.left.parent = null;
				toDelete.left.parent = toDelete;
				up = toDelete;
			} else {
				if (parent(toDelete).right.equals(toDelete)) {
					parent(toDelete).right = toDelete.left;
				} else {
					parent(toDelete).left = toDelete.left;
				}
				toDelete.left.parent = parent(toDelete);
				up = parent(toDelete);
				toDelete = null;
			}
		} else {
			AVLTree1 successor = search(successor(num));
			if (successor.right != null) {
				if (successor == successor.parent.left) {
					parent(successor).left = successor.right;
				} else {
					parent(successor).right = successor.right;
				}
				successor.right.parent = parent(successor);
				toDelete.data = successor.data;
				up = parent(successor);
				successor = null;
			}else{
				toDelete.data = successor.data ;
				up = parent(successor);
				successor.remove();
				successor = null;
			}
		}
		updateDel(up);
		return true;
	}

	public static void main(String args[]) {
//		AVLTree tree = new AVLTree();

		
//		tree.insert(15);
//		tree.insert(20);
//		tree.insert(24);
//		tree.insert(10);
//		tree.insert(13);
//		tree.insert(7);
//		tree.insert(30);
//		tree.insert(36);
//		tree.insert(25);
//		tree.delete(24);
//		tree.delete(20);
		
//		tree.insert(14);
//		tree.insert(17);
//		tree.insert(11);
//		tree.insert(7);
//		tree.insert(53);
//		tree.insert(4);
//		tree.insert(13);
//		tree.inTraverse();
//		System.out.println();
//		tree.insert(12);
//		tree.inTraverse();
//		System.out.println();
//		tree.insert(8);
//		tree.inTraverse();
//		System.out.println();
//		tree.delete(53);
//		tree.inTraverse();
//		System.out.println();
//		tree.delete(11);
//		tree.inTraverse();
//		System.out.println();
//		tree.delete(8);
//		tree.inTraverse();
//		System.out.println();

//		 tree.insert(3);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(5);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(9);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(1);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(0);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(2);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(6);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(10);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(7);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(4);
//		 tree.inTraverse();
//		 System.out.println();
//		 tree.insert(8);
//		 tree.inTraverse();
//		 System.out.println();
//		
//		 System.out.println(tree.getMin());
//		 System.out.println(tree.getMax());
//		
//		 System.out.println(tree.successor(3));
//		 tree.delete(10);
//		
//		 tree.inTraverse();
//		 System.out.println();
//		
//		 System.out.println(tree.predecessor(8));
//		 tree.inTraverse();

	
	}

}
