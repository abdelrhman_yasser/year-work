package stack.cs42;

import stack.IExpressionEvaluator;
/**
 * 
 * @author abdo
 *
 */
public class Evaluator implements IExpressionEvaluator {
    @Override
    public final String infixToPostfix(final String expression) {
        StringBuilder bd = new StringBuilder();
        MyStack operands = new MyStack();
        Boolean flag = false;
        Check(expression);
        for (int i = 0; i < expression.length(); i++) {
            if (!Character.isDigit(expression.charAt(i))) {
                if (flag) {
                    bd.append(' ');
                }
                flag = false;
                switch (expression.charAt(i)) {
                case '+':
                    if (operands.top == 0 || (char) operands.peek() == '(') {
                        // System.out.println("1 "+operands.top);
                        operands.push('+');
                    } else {
                        // System.out.println("2 "+(char) operands.peek());
                        while (operands.top != 0
                                && (char) operands.peek() != '(') {
                            bd.append((char) operands.pop());
                            bd.append(' ');
                        }
                        operands.push('+');
                    }
                    break;
                case '*':
                    if (operands.top == 0 
                    || (char) operands.peek() == '(' 
                    || (char) operands.peek() == '+'
                    || (char) operands.peek() == '-') {
                        operands.push(expression.charAt(i));
                    } else {
                        if (!operands.isEmpty()) {
                            bd.append((char) operands.pop());
                        }
                        operands.push('*');
                        bd.append(' ');
                    }
                    break;
                case '-':
                    if (operands.top == 0 || (char) operands.peek() == '(') {
                        // System.out.println("1 "+operands.top);
                        operands.push('-');
                    } else {
                        // System.out.println("2 "+(char) operands.peek());
                        while (operands.top != 0 
                                && (char) operands.peek() != '(') {
                            bd.append((char) operands.pop());
                            bd.append(' ');
                        }
                        operands.push('-');
                    }
                    break;
                case '/':
                    if (operands.top == 0 
                    || (char) operands.peek() == '(' 
                    || (char) operands.peek() == '+'
                    || (char) operands.peek() == '-') {
                        operands.push(expression.charAt(i));
                    } else {
                        if (!operands.isEmpty()) {
                            bd.append((char) operands.pop());
                        }
                        operands.push('/');
                        bd.append(' ');
                    }
                    break;
                case '(':
                    operands.push('(');
                    // System.out.println("check "+operands.peek());
                    break;
                case ')':
                    while ((char) operands.peek() != '(') {
                        bd.append((char) operands.pop());
                        bd.append(' ');
                    }
                    operands.pop();
                    break;
                case ' ':
                    break;
                default:
                    break;
                }
            } else {
                
                bd.append(expression.charAt(i));
                flag = true;
            }
            
        }
        while (!operands.isEmpty()) {
            bd.append(' ');
            bd.append((char) operands.pop());
        }
        return bd.toString();
    }
    @Override
    public final int evaluate(final String expression) {
        try {
            boolean flag = false;
            MyStack temp = new MyStack();
            StringBuilder temp1 = new StringBuilder();
            for (int i = 0; i < expression.length(); i++) {
                if (Character.isDigit(expression.charAt(i))) {
                    flag = true;
                    temp1.append(expression.charAt(i));
                } else if (expression.charAt(i) == ' ' && flag) {
                    // System.out.println(temp1.toString());
                    temp.push(temp1.toString());
                    temp1.delete(0, temp1.length());
                } else if (expression.charAt(i) == '+' 
                        || expression.charAt(i) == '-' 
                        || expression.charAt(i) == '*'
                        || expression.charAt(i) == '/') {
                    flag = false;
                    Float var2 = Float.valueOf(temp.pop().toString());
                    // System.out.println(var2);
                    Float var1 = Float.valueOf(temp.pop().toString());
                    // System.out.println(var1);
                    switch (expression.charAt(i)) {
                    case '+':
                        // System.out.println(String.valueOf(var1 + var2));
                        temp.push(String.valueOf(var1 + var2));
                        break;
                    case '-':
                        // System.out.println(String.valueOf(var1 - var2));
                        temp.push(String.valueOf(var1 - var2));
                        break;
                    case '*':
                        // System.out.println(String.valueOf(var1 * var2));
                        temp.push(String.valueOf(var1 * var2));
                        break;
                    case '/':
                        // System.out.println(String.valueOf(var1 / var2));
                        temp.push(String.valueOf(var1 / var2));
                        break;
                    default:
                        break;
                    }
                }
            }
            if (temp.size() != 1) {
                throw new RuntimeException();
            }
            return Math.round(Float.valueOf(temp.peek().toString()));
        } catch (NumberFormatException e) {
            throw new RuntimeException();
        }
    }
    /**
     * 
     * @param input it takes string input
     */
    public final void Check(final String input) {
        if (input.charAt(0) == '+' || input.charAt(0) == '*' 
           || input.charAt(0) == '-' || input.charAt(0) == '/') {
            throw new RuntimeException();
        } else if (input.charAt(input.length() - 1) == '+' 
                || input.charAt(input.length() - 1) == '*'
                || input.charAt(input.length() - 1) == '-' 
                || input.charAt(input.length() - 1) == '/') {
            throw new RuntimeException();
        }
        int num = 0, op = 0;
        MyStack check = new MyStack();
        boolean flag = true;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                if (i == input.length() - 1 
                        || !Character.isDigit(input.charAt(i + 1))) {
                    num++;
                }
            } else if (input.charAt(i) == '+' 
                    || input.charAt(i) == '*' 
                    || input.charAt(i) == '-'
                    || input.charAt(i) == '/') {
                op++;
            } else if (input.charAt(i) == '(') {
                check.push(input.charAt(i));
            } else if (input.charAt(i) == ')') {
                flag = true;
                if (!check.isEmpty() && (char) check.peek() == '(') {
                    check.pop();
                    if (i < input.length() - 1 
                            && Character.isDigit(input.charAt(i + 1))) {
                        throw new RuntimeException();
                    }
                }
            }
        }
        if (check.top != 0) {
            throw new RuntimeException();
        }
        System.out.println(op + " " + num);
        if (op + 1 != num) {
            throw new RuntimeException();
        }

    }
}
