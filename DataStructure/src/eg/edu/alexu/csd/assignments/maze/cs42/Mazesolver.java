package eg.edu.alexu.csd.assignments.maze.cs42;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.util.Pair;
import queue.cs42.Lqueue;
import stack.cs42.MyStack;

/**
 * 
 * @author abdo
 *
 */
public class Mazesolver implements eg.edu.alexu.csd.assignments.maze.IMazeSolver {

    @Override
    public final int[][] solveBFS(final File maze) {
        ArrayList<ArrayList<Integer>> seq = new ArrayList<>();
        try {
            boolean flagend = true;
            Scanner scan = new Scanner(maze);
            int x = scan.nextInt();
            int y = scan.nextInt();
            int cin = 0;
            int copos = 0;
            String[] grid = new String[x];
            Pair<Integer, Integer> intial = new Pair<Integer, Integer>(-1, -1);
            Pair<Integer, Integer> end = new Pair<Integer, Integer>(-1, -1);
            scan.nextLine();
            for (int i = 0; i < x; i++) {
                String temp = scan.nextLine();
                grid[i] = temp;
                if (temp.length() != y) {
                    throw new RuntimeException("mixshaped array line size : " + temp.length() + "lines num : " + y);
                }
            }
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length(); j++) {
                    if (grid[i].charAt(j) == 'S') {
                        cin++;
                        copos++;
                        intial = new Pair<Integer, Integer>(i, j);
                    }
                    if (grid[i].charAt(j) == 'E') {
                        end = new Pair<Integer, Integer>(i, j);
                        copos++;
                    }
                    if (grid[i].charAt(j) == '.') {
                        copos++;
                    }
                }
            }
            if ((intial.getKey() == -1 && intial.getValue() == -1) || (end.getKey() == -1 && end.getValue() == -1)) {
                throw new RuntimeException("intial point or end point isn't found !");
            } else if (cin > 1) {
                throw new RuntimeException("multiple intial points");
            }
            boolean[][] visited = new boolean[grid.length][grid[0].length()];
            Object[][] parents = new Object[grid.length][grid[0].length()];
            Lqueue intersection = new Lqueue();
            boolean flagfirst = false;
            intersection.enqueue(intial);
            while (flagend) {
                Pair<Integer, Integer> curnode = (Pair<Integer, Integer>) intersection.dequeue();
                visited[curnode.getKey()][curnode.getValue()] = true;
                if(intersection.isEmpty() && copos==0 && flagfirst){
                    return null ;
                }
                flagfirst = true;
                if (grid[curnode.getKey()].charAt(curnode.getValue()) == 'E') {
                    end = new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue());
                    flagend = false;
                    break;
                } else {
                    if (curnode.getValue() - 1 >= 0 && grid[curnode.getKey()].charAt(curnode.getValue() - 1) != '#'
                            && !visited[curnode.getKey()][curnode.getValue() - 1]) {
                        intersection.enqueue(new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue() - 1));
                        parents[curnode.getKey()][curnode.getValue() - 1] = curnode;
                        visited[curnode.getKey()][curnode.getValue() - 1] = true;
                        copos--;
                    }
                    if (curnode.getKey() + 1 < grid.length
                            && grid[curnode.getKey() + 1].charAt(curnode.getValue()) != '#'
                            && !visited[curnode.getKey() + 1][curnode.getValue()]) {
                        intersection.enqueue(new Pair<Integer, Integer>(curnode.getKey() + 1, curnode.getValue()));
                        parents[curnode.getKey() + 1][curnode.getValue()] = curnode;
                        visited[curnode.getKey() + 1][curnode.getValue()] = true;
                        copos--;

                    }
                    if (curnode.getValue() + 1 < grid[0].length()
                            && grid[curnode.getKey()].charAt(curnode.getValue() + 1) != '#'
                            && !visited[curnode.getKey()][curnode.getValue() + 1]) {
                        intersection.enqueue(new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue() + 1));
                        parents[curnode.getKey()][curnode.getValue() + 1] = curnode;
                        visited[curnode.getKey()][curnode.getValue() + 1] = true;
                        copos--;
                    }
                    if (curnode.getKey() - 1 >= 0 && grid[curnode.getKey() - 1].charAt(curnode.getValue()) != '#'
                            && !visited[curnode.getKey() - 1][curnode.getValue()]) {
                        intersection.enqueue(new Pair<Integer, Integer>(curnode.getKey() - 1, curnode.getValue()));
                        parents[curnode.getKey() - 1][curnode.getValue()] = curnode;
                        visited[curnode.getKey() - 1][curnode.getValue()] = true;
                        copos--;
                    }
                    if(intersection.isEmpty()){
                        return null ;
                    }
                }
            }
            ArrayList<Integer> temp = new ArrayList<>();
            int i = end.getKey();
            int j = end.getValue();
            temp.add(i);
            temp.add(j);
            // System.out.println("end "+temp.get(0)+";"+temp.get(1));
            seq.add(temp);
            temp = new ArrayList<>();
            // System.out.println("get
            // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
            while (!parents[i][j].equals(intial)) {
                int tempi = i;
                int tempj = j;
                i = ((Pair<Integer, Integer>) parents[tempi][tempj]).getKey();
                j = ((Pair<Integer, Integer>) parents[tempi][tempj]).getValue();
                temp.add(i);
                temp.add(j);
                // System.out.println("end "+temp.get(0)+";"+temp.get(1));
                seq.add(temp);
                temp = new ArrayList<>();
                // System.out.println("get
                // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
            }
            i = intial.getKey();
            j = intial.getValue();
            temp.add(i);
            temp.add(j);
            // System.out.println("end "+temp.get(0)+";"+temp.get(1));
            seq.add(temp);
            temp = new ArrayList<>();
            // System.out.println("get
            // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        int[][] ret = new int[seq.size()][2];
        for (int i = seq.size() - 1; i >= 0; i--) {
            ret[seq.size() - 1 - i][0] = seq.get(i).get(0);
            ret[seq.size() - 1 - i][1] = seq.get(i).get(1);
            // System.out.println(seq.get(i).get(0)+";"+seq.get(i).get(1));
        }
        return ret;

    }

    @Override
    public final int[][] solveDFS(final File maze) {
        ArrayList<ArrayList<Integer>> seq = new ArrayList<>();
        try {
            boolean flagend = true;
            Scanner scan = new Scanner(maze);
            int x = scan.nextInt();
            int y = scan.nextInt();
            int cin = 0;
            String[] grid = new String[x];
            Pair<Integer, Integer> intial = new Pair<Integer, Integer>(-1, -1);
            Pair<Integer, Integer> end = new Pair<Integer, Integer>(-1, -1);
            scan.nextLine();
            for (int i = 0; i < x; i++) {
                String temp = scan.nextLine();
                grid[i] = temp;
                if (temp.length() != y) {
                    throw new RuntimeException("mixshaped array line size : " + temp.length() + "lines num : " + y);
                }
            }

            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length(); j++) {
                    if (grid[i].charAt(j) == 'S') {
                        cin++;
                        intial = new Pair<Integer, Integer>(i, j);
                    }
                    if (grid[i].charAt(j) == 'E') {
                        end = new Pair<Integer, Integer>(i, j);
                    }
                }
            }
            if ((intial.getKey() == -1 && intial.getValue() == -1) || (end.getKey() == -1 && end.getValue() == -1)) {
                throw new RuntimeException("intial point or end point isn't found !");
            } else if (cin > 1) {
                throw new RuntimeException("multiple intial points");
            }
            boolean[][] visited = new boolean[grid.length][grid[0].length()];
            Object[][] parents = new Object[grid.length][grid[0].length()];
            MyStack intersection = new MyStack();
            intersection.push(intial);
            boolean flagfirst = false;
            while (flagend) {
                Pair<Integer, Integer> curnode = (Pair<Integer, Integer>) intersection.peek();
                visited[curnode.getKey()][curnode.getValue()] = true;
                if (curnode.getKey().equals(intial.getKey()) && curnode.getValue().equals(intial.getValue())
                        && flagfirst) {
                    return null;
                }
                flagfirst = true;
                if (grid[curnode.getKey()].charAt(curnode.getValue()) == 'E') {
                    end = new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue());
                    flagend = false;
                    break;
                } else {
                    int unvisited = 0;
                    if (curnode.getValue() - 1 >= 0 && grid[curnode.getKey()].charAt(curnode.getValue() - 1) != '#'
                            && !visited[curnode.getKey()][curnode.getValue() - 1]) {
                        intersection.push(new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue() - 1));
                        parents[curnode.getKey()][curnode.getValue() - 1] = curnode;
                        // System.out.println("parent :"+curnode+" node : " +new
                        // Pair<Integer, Integer>(curnode.getKey(),
                        // curnode.getValue() - 1));
                        unvisited++;
                    }
                    if (curnode.getKey() + 1 < grid.length
                            && grid[curnode.getKey() + 1].charAt(curnode.getValue()) != '#'
                            && !visited[curnode.getKey() + 1][curnode.getValue()]) {
                        intersection.push(new Pair<Integer, Integer>(curnode.getKey() + 1, curnode.getValue()));
                        parents[curnode.getKey() + 1][curnode.getValue()] = curnode;
                        // System.out.println("parent :"+curnode+" node : " +new
                        // Pair<Integer, Integer>(curnode.getKey()+1,
                        // curnode.getValue()));
                        unvisited++;
                    }
                    if (curnode.getValue() + 1 < grid[0].length()
                            && grid[curnode.getKey()].charAt(curnode.getValue() + 1) != '#'
                            && !visited[curnode.getKey()][curnode.getValue() + 1]) {
                        intersection.push(new Pair<Integer, Integer>(curnode.getKey(), curnode.getValue() + 1));
                        parents[curnode.getKey()][curnode.getValue() + 1] = curnode;
                        // System.out.println("parent :"+curnode+" node : " +new
                        // Pair<Integer, Integer>(curnode.getKey(),
                        // curnode.getValue()+1));
                        unvisited++;
                    }
                    if (curnode.getKey() - 1 >= 0 && grid[curnode.getKey() - 1].charAt(curnode.getValue()) != '#'
                            && !visited[curnode.getKey() - 1][curnode.getValue()]) {
                        intersection.push(new Pair<Integer, Integer>(curnode.getKey() - 1, curnode.getValue()));
                        parents[curnode.getKey() - 1][curnode.getValue()] = curnode;
                        // System.out.println("parent :"+curnode+" node : " +new
                        // Pair<Integer, Integer>(curnode.getKey() - 1,
                        // curnode.getValue()));
                        unvisited++;
                    }
                    if (unvisited == 0) {
                        intersection.pop();
                    }
                    if(intersection.isEmpty()){
                        return null ;
                    }
                }
            }
            ArrayList<Integer> temp = new ArrayList<>();
            int i = end.getKey();
            int j = end.getValue();
            temp.add(i);
            temp.add(j);
            // System.out.println("end "+temp.get(0)+";"+temp.get(1));
            seq.add(temp);
            temp = new ArrayList<>();
            // System.out.println("get
            // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
            while (!parents[i][j].equals(intial)) {
                int tempi = i;
                int tempj = j;
                i = ((Pair<Integer, Integer>) parents[tempi][tempj]).getKey();
                j = ((Pair<Integer, Integer>) parents[tempi][tempj]).getValue();
                temp.add(i);
                temp.add(j);
                // System.out.println("end "+temp.get(0)+";"+temp.get(1));
                seq.add(temp);
                temp = new ArrayList<>();
                // System.out.println("get
                // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
            }
            i = intial.getKey();
            j = intial.getValue();
            temp.add(i);
            temp.add(j);
            // System.out.println("end "+temp.get(0)+";"+temp.get(1));
            seq.add(temp);
            temp = new ArrayList<>();
            // System.out.println("get
            // "+seq.get(seq.size()-1).get(0)+":"+seq.get(seq.size()-1).get(1));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        int[][] ret = new int[seq.size()][2];
        for (int i = seq.size() - 1; i >= 0; i--) {
            ret[seq.size() - 1 - i][0] = seq.get(i).get(0);
            ret[seq.size() - 1 - i][1] = seq.get(i).get(1);
            // System.out.println(seq.get(i).get(0)+";"+seq.get(i).get(1));
        }
        return ret;

    }

}
