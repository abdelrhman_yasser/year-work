import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class nHasher {

	private static nHasher instance;
	private ArrayList<secondaryLevel> levels = new ArrayList<>();
	private ArrayList<Integer> rnums = new ArrayList<>();
	private FileOutputStream out ;
	private Random r = new Random();
	
	private int max=0,k, p, size;

	private nHasher() {
	}

	public static nHasher getInst() {
		if (instance == null)
			instance = new nHasher();
		return instance;
	}

	public void hash(String path , String outPath) {
		try {
			out = new FileOutputStream(new File(outPath));
			ReadNumbers(path);
			p = PrimeGenerator.getInstance().getPrimeGreaterN(max);
			addLevels();
			chooseK();
			addNums();
			hashLevels();
			printLevels();
			out.close();
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void addLevels(){
		for(int i=0 ; i<rnums.size() ; i++)
			levels.add(new secondaryLevel());
	}
	
	private void printLevels () throws IOException{
		for(int i=0 ; i<levels.size() ; i++){
			String s = "Level "+i+" " ;  
			out.write(s.getBytes());
			levels.get(i).printLevel(out);
			s = "\n" ;
			out.write(s.getBytes());
		}
	}
	
	private void hashLevels() throws Exception{
		for(secondaryLevel i : levels){
			while(!i.tri()){
			}
		}
	}
	
	private void chooseK() throws IOException{
		k = -1;
		while (k <= 0)
			k = r.nextInt(10000);
		
		String s = "First level >> k = " + k + " P = " + p + " \n";
		out.write(s.getBytes());
	}
	
	private void addNums(){
		for(int i: rnums){
			int key = (int)(((long)(k%p)*(long)(i%p))%p)%rnums.size() ;
			levels.get(key).add(i);
		}
	}

	private void ReadNumbers(String filPath) throws IOException {
		Scanner scanner = new Scanner(new File(filPath));
		String [] temps = scanner.nextLine().split(",");
		for (int i=0; i<temps.length; i++){
			int number = Integer.valueOf(temps[i]);
			max = Math.max(number, max);
			rnums.add(number);
		}
	}

}
