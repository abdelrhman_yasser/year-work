package Bubblesort;

import java.util.Random;

public class sort {
	
	int[] array ; 
	
	public sort(int n){
		array = new int[n] ;
		Random rand = new  Random();
		for(int i=0 ; i<n ; i++){
			array[i] = rand.nextInt(1000);
		}
	}
	
	public void print(){
		for(int i=0 ; i<array.length ; i++){
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}
	
	public void bubbleSort() {
		int len = array.length;
		boolean flag = false;
		for (int i=0; i<len; i++) {
			flag = false;
			for (int j=0; j<len-i-1; j++) {
				if (array[j] > array[j+1]) {
					swap(array, j, j+1);
					flag = true;
				}
			}
			if (!flag) {
				break;
			}
		}
	}
	
	private void swap(int array[], int ind1, int ind2) {
		int temp = array[ind1];
		array[ind1] = array[ind2];
		array[ind2] = temp;
	}
	
	public static void main(String args[]){
		sort data = new sort(100000);
//		data.print();
		double time = System.currentTimeMillis();
		data.bubbleSort();
		System.out.println((System.currentTimeMillis()-time)/1000+" sec");
//		data.print();
	}


}
