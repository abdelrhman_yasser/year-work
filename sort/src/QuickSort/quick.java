package QuickSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class quick {
	
	public static void main(String args[]){
		ArrayList<Integer> arrs = new ArrayList<>(Arrays.asList(1,5,4,6,4));	
		int[] arr = new int[10000000];
		Random rand = new Random();
		for(int i=0 ; i<10000000 ; i++){
			arr[i] = rand.nextInt(10002);	
		}
//		System.out.println("Array is : \n");
//		for(int i:arr)System.out.print(i+" ");
		double time = System.currentTimeMillis();
		sort(arr,0,arr.length-1);
		System.out.println("\nsort took "+((System.currentTimeMillis()-time)/1000) + " sec\n");
//		System.out.println("\nSorted array is : \n");
//		for(int i:arr)System.out.print(i+" ");
	}
	
	public static void sort(int[] arr,int p , int r){
		if(p<r){
			int pivotIndex = partition(arr,p,r);
			sort(arr,p,pivotIndex-1);
			sort(arr,pivotIndex+1,r);
		}
	}
	
	private static void swap(int[] arr, int i , int j){
		int temp = arr[i] ;
		arr[i] = arr[j] ;
		arr[j] = temp ;
	}
	
	public static int partition(int[] arr , int p , int r){
		int pivot = arr[r] , i = p-1;
		for(int j=p ; j<r ; j++){
			if(arr[j]<=pivot){
				i++;
				swap(arr,i,j);
			}
		}
		swap(arr,i+1,r);
		return i+1;
	}
	
}
